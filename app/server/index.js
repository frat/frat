const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const path = require('path');
const { initDB, close: closeDB } = require("./util/db");

const todosController = require('./controller/Todos')
const manualsController = require('./controller/Manuals')
const accountController = require('./controller/Account')
const integrationsController = require('./controller/Integrations')
const groupController = require('./controller/Group')
const tenantsController = require('./controller/Tenants')
const wikiController = require('./controller/Wiki')
const planController = require('./controller/Payments')
const monitorController = require('./controller/Mointors')

Object.defineProperty(Array.prototype, 'flat', {
    value: function (depth = 1) {
        return this.reduce(function (flat, toFlatten) {
            return flat.concat((Array.isArray(toFlatten) && (depth > 1)) ? toFlatten.flat(depth - 1) : toFlatten);
        }, []);
    }
});

const app = express()
var http = require('http').Server(app);
const io = require('socket.io')(http);
const chat = require("./controller/Chat");
chat(io);

app.use(helmet());

app.use(bodyParser.json({ limit: '10mb' }))
app.use(express.urlencoded({ extended: true, limit: '10mb' }));
app.use(compression({ level: 9 }));
app.use(express.static(path.resolve(__dirname, '../static')))
app.use(accountController.accountAuthMiddleWare);
app.use((err, req, res, next) => {
    console.log(`********* Global error ${req.originalUrl}, ${JSON.stringify(req.requestTenant)}, ${JSON.stringify(req.requestAccount)} ${err}:${err.stack}`);
    res.status(500).send({ error: err });
})

todosController(app);
manualsController(app);
accountController(app);
integrationsController(app);
groupController(app);
tenantsController(app);
wikiController(app);
planController(app);
monitorController(app, chat);

app.get('*', function (req, res) {
    res.status(200).sendFile(path.resolve(__dirname, "../static/index.html"));
});
const forExport = {
    suspendFirstStart: false,
    async start(port, callback) {
        await initDB();
        return new Promise((resolve, reject) => {
            try {
                process.server = http.listen(port, () => {
                    if (callback) {
                        callback(port);
                    }
                    resolve();
                });
            } catch (e) {
                reject(e);
            }
        })
    },
    async stop(callback) {
        return new Promise(async (resolve, reject) => {
            var errors = [];
            try {
                await closeDB()
            } catch (e) {
                errors.push(e)
            }
            try {
                require("./controller/Chat").shutdown();
            } catch (e) {
                errors.push(e)
            }
            try {
                io.close();
            } catch (e) {
                errors.push(e)
            }
            try {
                http.close(() => {
                    if (callback) { callback(); }
                    if (errors.length > 0) {
                        reject(errors);
                    } else {
                        resolve();
                    }
                });
            } catch (e) {
                errors.push(e)
            }
            if (errors.length > 0) {
                reject(errors[0]);
            }
        });
    }
}
process.nextTick(() => {
    if (!forExport.suspendFirstStart) {
        const port = process.env.PORT || process.port || 3000;
        forExport.start(port, () => {
            console.log(`server start with port ${port}`);
        });
    }
    forExport.suspendFirstStart = false;
})
module.exports = forExport;
