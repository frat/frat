var messageJSON = require("../../../src/web/js/message.json");

module.exports = {
    getClientRanguage(req) {
        return req.acceptsLanguages('ja') || 'en';
    },
    getMessage(lang, key, params) {
        var keys = key.split(".");
        var top = messageJSON[lang];
        if (!top) {
            top = messageJSON['en'];
        }
        var message = top;
        for (let i = 0; i < keys.length; i++) {
            message = message[keys[i]];
            if (!message) {
                return key;
            }
        }
        if (params) {
            for (param of Object.keys(params)) {
                message = message.replace(new RegExp("\\{" + param + "\\}", "g"), params[param]);
            }
        }

        return message;
    }
}