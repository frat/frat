const Storage = require("../util/storage");
var Strings = require("../util/stringUtil");
var typeorm = require("typeorm");
var connection;

module.exports = {
    async initDB() {
        connection = await typeorm.createConnection({
            type: "postgres",
            host: process.env.DB_HOST || "localhost",
            port: 5432,
            username: process.env.DB_USER || "root",
            password: process.env.DB_PASS || "root",
            database: process.env.DB_DB || "postgres",
            synchronize: true,
            logging: !!process.env.DB_DEBUG || false,
            entities: [
                require("../model/Account").schema,
                require("../model/AccountAccessKey").schema,
                require("../model/Manual").schema,
                require("../model/Todo").schema,
                require("../model/Group").schema,
                require("../model/TodoOrder").schema,
                require("../model/CustomAttribute").schema,
                require("../model/CustomAttributeValue").schema,
                require("../model/TodoTodoRelation").schema,
                require("../model/Tenant").schema,
                require("../model/AccountTenantRelation").schema,
                require("../model/File").schema,
                require("../model/TodoTemplate").schema,
                require("../model/SprintBar").schema,
                require("../model/Event").schema,
                require("../model/ChatMessage").schema,
                require("../model/Wiki").schema,
                require("../model/Integration").schema,
                require("../model/MessageLink").schema,
                require("../model/PushedEvent").schema,
                require("../model/Plan").schema
            ]
        });

        await Promise.all(require("../model/Tenant").defaultDomains.map(async (domain) => {
            if (!await module.exports.findOne(null, require("../model/Tenant").table, "key=:key", { key: domain })) {
                await Storage.createBucket(domain);
                await module.exports.save(require("../model/Tenant").table, { key: domain });
                console.log(`domain "${domain}" created`);
            } else {
                console.log(`domain "${domain}" may aleady exists.`);
            }
        }));
        try {
            await Storage.createAccountIconBucket();
        } catch (e) {
            console.log(`Account icon bucket may aleady exists. :${e}`);
        }
    },
    async isActive() {
        return await connection.query("select 1");
    },
    async close() {
        await connection.close();
    },
    async save(name, object) {
        var time = new Date();
        if (!object.id) {
            if (Array.isArray(object)) {
                object.forEach(o => o.createdAt = time);
            } else {
                object.createdAt = time;
            }
        }
        if (Array.isArray(object)) {
            object.forEach(o => {
                o.updatedAt = time;
                if (object.id) {
                    delete o.createdAt;
                }
            });
        } else {
            object.updatedAt = time;
            if (object.id) {
                delete object.createdAt;
            }
        }
        return await connection.getRepository(name).save(object);
    },
    async findById(name, id) {
        const obj = await connection.getRepository(name).findOne(id);
        if (!obj || obj.deletedAt) {
            return null;
        }
        return obj;
    },
    async find(tenant, name, query, parameter, limit, offset, orderBy) {
        if (!parameter) {
            parameter = {};
        }
        if (tenant) {
            parameter.tenantId = tenant.id;
        }
        var query = await connection.getRepository(name).createQueryBuilder(name)
            .where(query + ` and "${name}"."deletedAt" IS NULL ${tenant ? 'and "' + name + '"."tenantId"=:tenantId' : ""}`, parameter)
        if (limit) {
            query.limit(limit);
        }
        if (offset) {
            query.offset(offset);
        }
        if (orderBy) {
            query.orderBy(...orderBy.split(" "));
        } else {
            query.orderBy(`"${name}"."createdAt"`, "DESC");
        }
        return await query.getMany();
    },
    async findOne(tenant, name, query, parameter) {
        if (!parameter) {
            parameter = {}
        }
        if (tenant) {
            parameter.tenantId = tenant.id;
        }
        return await connection.getRepository(name).createQueryBuilder(name)
            .where(query + ` and "${name}"."deletedAt" IS NULL ${tenant ? 'and "' + name + '"."tenantId"=:tenantId' : ""}`, parameter)
            .getOne();
    },
    async rawQuery(name, query, param) {
        return await connection.getRepository(name).query(query, param);
    },
    async delete(name, obj) {
        const time = new Date();
        if (Array.isArray(obj)) {
            obj.forEach(o => o.createdAt = time);
        } else {
            obj.deletedAt = time;
        }
        await module.exports.save(name, obj);
    },
    async deleteConcrete(name, obj) {
        await module.exports.getConnection().getRepository(name).delete(obj.id);
    },
    async transaction(work) {
        const queryRunner = await connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        var error = null;
        try {
            await work(queryRunner);
            await queryRunner.commitTransaction();
        } catch (err) {
            error = err;
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
        if (error) {
            throw error;
        }
    },
    async createSequence(name, start) {
        if (!name.match(/^[a-zA-Z0-9\-]+$/)) {
            throw `sequence name "${name}" must only charactor`
        }
        if (start && !Strings.isInteger(start)) {
            throw `sequence start "${start}" must only charactor`
        }
        var executeCommand = `CREATE SEQUENCE "${name}"`;
        if (start) {
            executeCommand = executeCommand + " start " + start;
        }
        return await connection.query(executeCommand);
    },
    async nextSequenceValue(name) {
        const value = await connection.query(`select nextval('${name}')`);
        return value[0].nextval;
    },
    createQueryBuilder(name) {
        return connection.getRepository(name).createQueryBuilder(name);
    },
    getConnection() {
        return connection;
    }
}