var S3 = require('aws-sdk/clients/s3');
const BACKET_NAME_PREFIX = process.env.S3_BUCKETPREFIX || "fratbucketnamefor"
const ACCOUNT_ICON_BACKET_NAME_PREFIX = process.env.S3_ACCOUNT_BUCKETPREFIX || "accounticonbucketname"
const config = {
    "accessKeyId": process.env.S3_ACCESS_KEY || "MINIO_ACCESS_KEY",
    "secretAccessKey": process.env.S3_SECRET_KEY || "MINIO_SECRET_KEY",
    "region": process.env.S3_REGION || "ap-northeast-1",
    signatureVersion: 'v4',
}
if (!process.env.S3_ENDPOINT) {
    config.endpoint = 'http://localhost:9000';
    config.s3ForcePathStyle = true;
}

var s3 = new S3(config);
module.exports = {
    async createAccountIconBucket() {
        await this.createBucket(null, true);
        const publicReadPolicy =
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "AddPerm",
                    "Effect": "Allow",
                    "Principal": "*",
                    "Action": ["s3:GetObject"],
                    "Resource": [`arn:aws:s3:::${ACCOUNT_ICON_BACKET_NAME_PREFIX}/*`]
                }
            ]
        }
        await new Promise((res, rej) => s3.putBucketPolicy({ Bucket: ACCOUNT_ICON_BACKET_NAME_PREFIX, Policy: JSON.stringify(publicReadPolicy) }, (err, data) => {
            if (err) {
                rej(err);
                return;
            }
            res(data);
        }));
    },
    async createBucket(tenantName, accountBucket) {
        var Bucket;
        var bucketconfig;
        if (accountBucket) {
            Bucket = ACCOUNT_ICON_BACKET_NAME_PREFIX;
            bucketconfig = { Bucket, ACL: "public-read" }
        } else {
            Bucket = BACKET_NAME_PREFIX + tenantName
            bucketconfig = { Bucket, ACL: "private" }
        }
        await s3.createBucket(bucketconfig).promise();
        if (!process.env.S3_ENDPOINT) {
            return;
        }
        await s3.putPublicAccessBlock({ Bucket, PublicAccessBlockConfiguration: { BlockPublicAcls: true, BlockPublicPolicy: true } }).promise();
        var param = { Bucket };
        param.CORSConfiguration = {
            CORSRules: [
                {
                    AllowedHeaders: ["*"],
                    AllowedMethods: ["POST", "GET", "DELETE", "HEAD"],
                    AllowedOrigins: ["*"],
                    ExposeHeaders: [],
                    MaxAgeSeconds: 3000
                }
            ]
        }
        await s3.putBucketCors(param).promise();
    },
    urlToKey(bucketname, url) {
        let key = url.substring((url.substring(0, url.indexOf("/", 10)) + "/" + bucketname + "/").length);
        return decodeURIComponent(key);
    },
    async getSignedUrl(tenantName, url) {
        var Bucket = BACKET_NAME_PREFIX + tenantName;
        var key = this.urlToKey(Bucket, url);
        var url = await s3.getSignedUrl('getObject', {
            Bucket,
            Key: key,
            Expires: 60
        });
        return url;
    },

    async getAccountIconUploadUrl(key, filesize) {
        return await this.getUploadUrl(null, key, filesize, true);
    },
    async getUploadUrl(tenantName, key, filesize, forAccountIcon) {
        var Bucket;
        if (forAccountIcon) {
            Bucket = ACCOUNT_ICON_BACKET_NAME_PREFIX;
        } else {
            Bucket = BACKET_NAME_PREFIX + tenantName;
        }
        return await s3.createPresignedPost({
            Bucket,
            Fields: {
                key: key
            },
            Conditions: [
                ['content-length-range', 0, filesize],
                { 'acl': forAccountIcon ? 'public-read' : 'private' }
            ],
            Expires: 60
        });
    },
    async deleteAccountIcon(url) {
        return await this.delete(null, url, true);
    },
    async delete(tenantName, url, forAccountIcon) {
        var Bucket;
        if (forAccountIcon) {
            Bucket = ACCOUNT_ICON_BACKET_NAME_PREFIX;
        } else {
            Bucket = BACKET_NAME_PREFIX + tenantName
        };
        var key = this.urlToKey(Bucket, url);
        await s3.deleteObject({ Bucket, Key: key })
    }
};