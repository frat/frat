
const BigNumber = require('bignumber.js');
BigNumber.config({ RANGE: [-1000, 1], EXPONENTIAL_AT: [-1000, 1], DECIMAL_PLACES: 1000, ALPHABET: '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' })//Duplicate Todos.js
const defaultAndIncrementsStringValue = "0.00000000000000000000000000000000001";//Duplicate Todos.js
const ORDERDEFAULT = new BigNumber(defaultAndIncrementsStringValue);//Duplicate Todos.js
const ORDERINCREMENTS = new BigNumber(defaultAndIncrementsStringValue);//Duplicate Todos.js

module.exports = (beforeOrderKey, afterOrderKey) => {
    let returnOrderKey;
    if (beforeOrderKey && afterOrderKey) {
        returnOrderKey = new BigNumber(beforeOrderKey).plus(new BigNumber(afterOrderKey)).dividedBy(new BigNumber(2)).toPrecision(1000);
    } else if (!beforeOrderKey) {
        returnOrderKey = new BigNumber(afterOrderKey).div(2).toPrecision(1000);
    } else if (!afterOrderKey) {
        returnOrderKey = new BigNumber(beforeOrderKey).plus(ORDERINCREMENTS).toPrecision(1000);
    }
    return returnOrderKey || ORDERDEFAULT.toPrecision(1000);

}
module.exports.aIsLargerThanB = (a, b) => {
    return new BigNumber(a).gt(new BigNumber(b));
}
module.exports.ORDERDEFAULT = ORDERDEFAULT.toPrecision(1000);