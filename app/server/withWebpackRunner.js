const webpack = require('webpack');
console.log("run with compile webpack");
var startTime = new Date().getTime();
const webpackConfig = require('../../webpack.config')
webpack(webpackConfig, (err, stats) => {
    if (err || stats.hasErrors()) {
        console.log("webpack failed " + (err || stats));
        return;
    }
    console.log("webpack compiled cost " + (new Date().getTime() - startTime) + "ms");
    require("./index");
});