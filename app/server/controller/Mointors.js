const db = require("../util/db");

module.exports = function (app, chat) {
    app.get('/api/monitor', async function (req, res) {
        try {
            await db.isActive();
            await chat.isActive();
            res.send({ status: "ok" });
        } catch (e) {
            res.status(500).send({ status: "error", message: e + "" });
        }
    })
}