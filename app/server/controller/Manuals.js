const { Manual, table } = require("../model/Manual");
const { Account, table: accountTable } = require("../model/Account");
const { Group } = require("../model/Group");
const db = require("../util/db");

module.exports = function (app) {
    app.get('/api/manuals/:groupId', async function (req, res) {
        if (!await Group.isMember(req.requestTenant, req.params.groupId, req.requestAccount)) {
            res.status(409).send();
            return;
        }
        var manuals = await db.createQueryBuilder(table).leftJoinAndSelect(table + ".register", accountTable)
            .where(`${table}."groupId" = :groupId and ${table}.deletedAt IS NULL`, { groupId: req.params.groupId })
            .getMany();
        res.send(manuals);
    })
    app.post('/api/manuals', async function (req, res) {
        var manual = req.body;
        manual.register = req.requestAccount;
        if (!manual.groupId) {
            res.status(400).send();
            return;
        }
        if (!await Group.isMember(req.requestTenant, manual.groupId, req.requestAccount)) {
            res.status(409).send();
            return;
        }
        await db.save(table, manual);
        res.send(manual);
    })
}