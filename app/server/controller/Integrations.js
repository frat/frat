const { table, type: IntegrationType } = require("../model/Integration");
const { Todo, table: tableTodo } = require("../model/Todo");
const { Event, type: EventType, table: tableEvent } = require("../model/Event");
const { MessageLink, table: tableMessageLink } = require("../model/MessageLink");
const ChatSender = require("./Chat").sender;
const crypto = require('crypto');

const db = require("../util/db");

module.exports = function (app) {
    const findKeysFromText = (text, groupKey) => {
        return text.match(new RegExp(`(close|closing)\\s*(${groupKey}-\\d+)`, "g"))
    }
    const execCompleteHook = async (tenant, integration, text) => {
        const matchedKey = findKeysFromText(text, integration.group.key);
        if (matchedKey && matchedKey.length > 0) {
            const todoKeys = matchedKey.map(m => m.replace("close ", "").replace("closing ", ""))
            const todos = await db.find(tenant, tableTodo, "key in (:...todoKeys)", { todoKeys });
            console.log(`log:completeWebHook\ttenant:${tenant.key}\tgroup:${integration.group.key}\ttodos:${todos.map(t => t.id).join(",")}`);
            if (todos.length == 0 || todos.some(t => t.groupId != integration.group.id)) {
                console.log(`log:rejectWebhook\tintegrationGroup:${integration.group.id}\ttodosGroup:${todos.map(t => t.groupId).join(",")}`);
                throw "wrong group";
            }
            const updateReturn = await db.getConnection().createQueryBuilder()
                .update(tableTodo)
                .set({ progress: 100 })
                .where('"groupId" = :groupId and key in (:...todoKeys)', { groupId: integration.group.id, todoKeys })
                .execute();
            console.log(`log:completeWebHookUpdated\ttenant:${tenant.key}\tupdateReturn:${JSON.stringify(updateReturn)}`);
            await Promise.all(todos.map(async (todo) => {
                const event = new Event({
                    tenant: tenant, todo, integration, type: EventType.TODO_UPDATE, summary:
                    {
                        type: EventType.TODO_UPDATE_PROGRESS,
                        changed: [{
                            title: "progress",
                            from: todo.progress,
                            to: 100,
                        }]
                    }
                });
                if (todo.groupId) {
                    event.groupId = todo.groupId;
                }
                const save = db.save(tableEvent, event);
                todo.lastEvent = event.summary;
                todo.progress = 100;
                todo.group = integration.group;
                ChatSender.sendToGroup(ChatSender.type.UPDATE_TODO_PROGRESS, todo);
                return save;
            }));
        }
    }
    const escapeJSON = (src) => {
        if (!src) {
            return src;
        }
        return src.split("\\").join("\\\\").split('"').join("\"")
    }
    const execLinkToCommand = async ({ tenant, protocol, host, todoKey, integration, messageUrl, comment }) => {
        var todoUrl = `${protocol}://${host}/ticket/${todoKey}`;
        const todo = await db.findOne(null, tableTodo, "key=:key", { key: todoKey });
        if (!todo) {
            throw "No such todo " + todoKey
        }
        if (todo.groupId != integration.group.id) {
            throw "Can't access to todo " + todoKey + ` todo.groupId=${todo.groupId}&=${integration.group.id}`;
        }
        const messageLink = new MessageLink({ tenant, todo, comment, url: messageUrl })
        await db.save(tableMessageLink, messageLink);
        const event = new Event({
            tenant, todo, integration, type: EventType.TODO_UPDATE, summary:
            {
                type: EventType.TODO_UPDATE,
                changed: [{
                    title: "messageLinks",
                    from: null,
                    to: "creted",
                }]
            }
        });
        if (todo.groupId) {
            event.groupId = todo.groupId;
        }
        await db.save(tableEvent, event);

        const todos = await Todo.loadTodo(tenant, integration.group, {
            param: { key: todoKey },
            sql: ` and "todo".key=:key`
        });
        await Todo.loadChildrenToJoinPersonnels(tenant, todos);
        const pushTodo = todos[0];

        pushTodo.lastEvent = event.summary;
        delete messageLink.todo
        pushTodo.newMessageLink = messageLink;
        pushTodo.group = integration.group;
        ChatSender.sendToGroup(ChatSender.type.UPDATE_TODO, pushTodo);
        return { todoKey, todoUrl, todoTitle: pushTodo.title };
    };
    const findHandler = (integration) => {
        if (!integration) {
            return null;
        }
        if (integration.type == IntegrationType.GITLAB) {
            return async (req) => {
                const body = req.body;
                if (body.object_kind == "merge_request" && body.object_attributes.action == "merge") {
                    await execCompleteHook(req.requestTenant, integration, body.object_attributes.description);
                }
            };
        } else if (integration.type == IntegrationType.AZURE_DEVOPS_INTEGRATION) {
            return async (req) => {
                const body = req.body;
                if (body.eventType == "git.pullrequest.updated" && body.resource.status == "completed") {
                    await execCompleteHook(req.requestTenant, integration, body.detailedMessage.text);
                }
            };
        } else if (integration.type == IntegrationType.GITHUB) {
            return async (req) => {
                const body = req.body;
                if (body.action == "closed" && body.pull_request.merged == true) {
                    await execCompleteHook(req.requestTenant, integration, body.pull_request.body);
                }
            };
        } else if (integration.type == IntegrationType.TEAMS_INTEGRATION) {
            return async (req, sharedSecret) => {
                const bufSecret = Buffer.from(sharedSecret, "base64");
                try {
                    var auth = req.header('authorization');
                    var msgBuf = Buffer.from(JSON.stringify(req.body), 'utf8');
                    var msgHash = "HMAC " + crypto.createHmac('sha256', bufSecret).update(msgBuf).digest("base64");
                    if (msgHash === auth) {
                        var receivedMsg = req.body;
                        if (!receivedMsg.text) {
                            throw "unknown format";
                        }
                        const commandLine = receivedMsg.text.split("\n").find(l => l.indexOf("<at>") >= 0 && l.indexOf("linkto") >= 0);
                        const messages = commandLine.substring(commandLine.indexOf("</at>") + 5).replace(/&nbsp;/g, ' ').split(" ").filter(t => t);
                        const command = messages.shift();
                        const todoKey = messages.shift();
                        const comment = messages.join(" ");
                        if (command.toLowerCase() == "linkto") {
                            const messageUrl = "https://teams.microsoft.com/l/message/" + receivedMsg.channelData.channel.id + "/" + receivedMsg.id
                            const commandResult = await execLinkToCommand({ tenant: req.requestTenant, protocol: req.protocol, host: req.get("Host"), todoKey, integration, messageUrl, comment });
                            return `{ "type": "message", "text": "This message linked to [${commandResult.todoKey}](${commandResult.todoUrl}) ${escapeJSON(commandResult.todoTitle)}" }`;
                        } else {
                            throw "No such command \"" + command.split("\"").join("\\\"") + "\"";
                        }
                    } else {
                        throw "Error: message sender cannot be authenticated.";
                    }
                } catch (err) {
                    throw "Error: " + err + "\n" + err.stack;
                }
            };
        } else if (integration.type == IntegrationType.SLACK_INTEGRATION) {
            return async (req) => {
                const messages = req.body.text.split(" ").filter(t => t);
                const slackHook = messages.shift();
                const command = messages.shift();
                const todoKey = messages.shift();
                const comment = messages.join(" ");
                if (command.toLowerCase() == "linkto") {
                    let messageUrl = `https://${req.body.team_domain}.slack.com/archives/` + req.body.channel_id + "/p" + (req.body.timestamp.split(".").join(""));
                    if (req.body.thread_ts) {
                        messageUrl += "?thread_ts=" + req.body.thread_ts;
                    }
                    const commandResult = await execLinkToCommand({ tenant: req.requestTenant, protocol: req.protocol, host: req.get("Host"), todoKey, integration, messageUrl, comment });
                    return `{ "type": "message", "text": "This message linked to <${commandResult.todoUrl}|${commandResult.todoKey}> ${escapeJSON(commandResult.todoTitle)}" }`;

                } else {
                    throw "No such command \"" + command.split("\"").join("\\\"") + "\"";
                }
            }
        }
    }
    app.post('/webhook/groups/:id/integrations/:type/:key', async function (req, res) {
        const integration = await db.createQueryBuilder(table)
            .innerJoinAndSelect(`${table}.group`, `group`)
            .where(`"${table}"."tenantId"=:tenantId and "${table}"."type"=:type and "${table}"."key"=:key and "${table}"."deletedAt" IS NULL `, { tenantId: req.requestTenant.id, type: req.params.type, key: req.params.key })
            .getOne();
        if (integration == null || !(req.params.id == integration.group.id || req.params.id == integration.group.key)) {
            console.log(`webhook error no fit integration integration="${JSON.stringify(integration)}, req.param=${JSON.stringify(req.params)}"`)
            res.status(404).send();
            return;
        }
        const handler = findHandler(integration);
        if (!handler) {
            res.status(404).send();
            return;
        }
        try {
            const message = await handler(req, integration.sharedSecret);
            res.send(message || null);
        } catch (e) {
            if (e) {
                console.log(`webhook error ${e} body ${JSON.stringify(req.body)}`);
            }
            res.status(400).send({ type: "message", text: e + ":" + e.stack });
        }
    })
};