const { Group, table, accountRelationType: GroupAccountRelationType } = require("../model/Group");
const { table: accountTable } = require("../model/Account");
const { Todo, table: todoTable, } = require("../model/Todo");
const { table: todoOrderTable } = require("../model/TodoOrder");
const { table: tableCustomAttribute } = require("../model/CustomAttribute");
const { table: tableCustomAttributeValue } = require("../model/CustomAttributeValue");
const { table: tableTodoTemplate } = require("../model/TodoTemplate");
const { table: tableMessageLink } = require("../model/MessageLink");
const { table: tableTodoOrder } = require("../model/TodoOrder");
const { SprintBar, table: tableSprintBar } = require("../model/SprintBar");
const { File, table: tableFile } = require("../model/File");
const { table: tableIntegration } = require("../model/Integration");
const { table: tableWiki } = require("../model/Wiki");
const { Tenant } = require("../model/Tenant");
const { Event, table: tableEvent, type: EventType } = require("../model/Event");
const { Plan } = require("../model/Plan");
const ChatSender = require("./Chat").sender;
const Storage = require("../util/storage");
const Strings = require("../util/stringUtil");
const BigNumber = require('bignumber.js');


const db = require("../util/db");
const { isUrl, isInteger } = require("../util/stringUtil");

module.exports = function (app) {
    app.get('/api/groups/self', async function (req, res) {
        var account = await db.createQueryBuilder(accountTable)
            .leftJoinAndSelect(accountTable + ".groups", "groups", `"tenantId"=:tenantId`, { tenantId: req.requestTenant.id })
            .where(`"${accountTable}".id = :id and "groups"."deletedAt" IS NULL`, { id: req.requestAccount.id })
            .getOne();
        var viewAccount = await db.createQueryBuilder(accountTable)
            .leftJoinAndSelect(accountTable + ".viewGroups", "viewGroups", `"tenantId"=:tenantId`, { tenantId: req.requestTenant.id })
            .where(`"${accountTable}".id = :id and "viewGroups"."deletedAt" IS NULL`, { id: req.requestAccount.id })
            .getOne();
        if (!account) {
            res.send([]);
            return;
        }
        var memberGroup = account.groups.map(g => { g.type = GroupAccountRelationType.MEMBER; return g; });
        var viewerGroup = viewAccount.viewGroups.map(g => { g.type = GroupAccountRelationType.VIEWER; return g; });
        res.send(memberGroup.concat(viewerGroup));
    })
    app.get('/api/groups/open', async function (req, res) {
        var groups = await Group.openGroupInTenant(req.requestTenant);
        res.send(groups);
    })
    const loadGroupInfo = async (req, res, onSuccess) => {
        if (!req.params.id) {
            res.status(400).send();
            return;
        }
        var groupIdOrKey = req.params.id;
        var group = await Group.findGroupWithMember(req.requestTenant, groupIdOrKey);
        if (!group || !group.members) {
            res.status(404).send();
        } else if (!await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
        } else {
            onSuccess(group);
        }
    }
    app.get('/api/groups/:id', async function (req, res) {
        loadGroupInfo(req, res, (group) => {
            res.send(group);
        })
    })
    app.get('/api/groups/:id/todos', async function (req, res) {
        loadGroupInfo(req, res, async (group) => {
            const WithCompleted = req.query.withCompleted;
            const completed = req.query.completed;
            const defaultSQL = ` and (${todoTable}."parentId" IS NULL or parent."groupId" != :groupId or parent."groupId" IS NULL)`;
            const queryParam = {
                param: { groupId: group.id },
                sql: defaultSQL
            }
            if (!WithCompleted) {
                //except completed
                const firstNotDone = await Todo.loadTodo(req.requestTenant, group, {
                    param: { groupId: group.id },
                    sql: defaultSQL + ` and ((${todoTable}.progress != 100 and ${todoTable}.progress != 0) or ((${todoTable}.progress = 0 or ${todoTable}.progress is null) and (children.progress != 100 or children.progress is null)))`
                }, 1);
                if (firstNotDone && firstNotDone.length == 1 && firstNotDone[0].order && firstNotDone[0].order[0] && firstNotDone[0].order[0].orderKey) {
                    const bars = await SprintBar.loadSprintBar(req.requestTenant, group, null, null, `"orderKey" DESC`);
                    const firstNotDoneOrderKey = firstNotDone[0].order[0].orderKey;
                    var beforeBar = bars.find(b => b.orderKey < firstNotDoneOrderKey);
                    queryParam.sql = defaultSQL + ` and "${todoOrderTable}"."orderKey" ${completed ? " < " : " >= "} :minOrderKey`
                    queryParam.param.minOrderKey = (beforeBar && beforeBar.orderKey) || firstNotDoneOrderKey;
                }
            }
            const todos = await Todo.loadTodo(req.requestTenant, group, queryParam);
            await Todo.loadChildrenToJoinPersonnels(req.requestTenant, todos);
            res.send(todos);
        })
    })
    app.get('/api/groups/:id/todos/search', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.id);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        var param = {}
        var sql = ""
        var customAttributeIndex = 0;
        for (key in req.query) {
            if (key == "q" || !req.query[key]) {
                continue;
            }
            if (key == "searchWord") {
                sql += ` and (${todoTable}.title like :searchWord or ${todoTable}.content like :searchWord or ${todoTable}.key like :searchWord)`
                param.searchWord = `%${req.query[key]}%`;
            } else if (key == "sprint") {
                var orderKeyBetween = await SprintBar.loadSprintOrderBetween(req.requestTenant, group, req.query[key])
                sql += ` and ("${todoOrderTable}"."orderKey" between :minOrderKey and :maxOrderKey)`
                param.maxOrderKey = orderKeyBetween.maxOrderKey;
                param.minOrderKey = orderKeyBetween.minOrderKey;
            } else if (key == "point") {
                sql += ` and ${todoTable}.point = :point`;
                param.point = req.query[key];
            } else if (key == "ignoreTodoIds") {
                sql += ` and "${todoTable}".id not in (:...ignoreTodoIds)`;
                param.ignoreTodoIds = req.query[key].split(",");
            } else if (key == "personnels") {
                sql += ` and "personnels".name in (:...personnelNames)`;
                param.personnelNames = req.query[key].split(",");
            } else if (key.indexOf("custom.") == 0) {
                customAttributeIndex++
                var customAttributeTitle = key.split(".")[1];
                const intValueQuery = (!Strings.isNumberFormat(req.query[key])) ? "" : ` or "${tableCustomAttributeValue}"."intValue" = :customAttributeValue${customAttributeIndex}`
                const dateValueQuery = (!Strings.isDateFormat(req.query[key])) ? "" : ` or "${tableCustomAttributeValue}"."dateValue" = :customAttributeValue${customAttributeIndex}`
                sql += ` and ( "${tableCustomAttribute}".title=:customAttributeTitle${customAttributeIndex} and ("${tableCustomAttributeValue}"."stringValue" = :customAttributeValue${customAttributeIndex}${intValueQuery}${dateValueQuery}))`;
                param["customAttributeTitle" + customAttributeIndex] = customAttributeTitle;
                param["customAttributeValue" + customAttributeIndex] = req.query[key];
            }
        }
        const todos = await Todo.loadTodo(req.requestTenant, group, {
            param: param,
            sql,
        }, req.query.limit)
        res.send(todos)
    })
    app.post('/api/groups/:id/uploadUrl', async function (req, res) {
        if (!req.params.id || !req.body.fileSize || !Strings.isNumberFormat(req.body.fileSize) || !req.body.fileName) {
            res.status(400).send();
            return;
        }
        if ("null" != req.params.id) {
            var group = await Group.findGroupWithMember(req.requestTenant, req.params.id);
            if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
                res.status(401).send();
                return;
            }
            const fileSize = (await db.getConnection().query(`select sum(size) as sum from file left join "group" on file."groupId" = "group".id where file."tenantId"=$1 and file."deletedAt" IS NULL and "group"."deletedAt" is null`, [req.requestTenant.id]))[0].sum;
            const plan = await Plan.loadPlan(req.requestTenant);
            if (new BigNumber(group.fileSizeLimit || req.requestTenant.fileSizeLimit || Plan.getFileSizeLimit(plan)).lt(new BigNumber(fileSize).plus(new BigNumber(req.body.fileSize)))) {
                res.status(432).send({ error: "file size to be " + (new BigNumber(fileSize).plus(new BigNumber(req.body.fileSize))) + " is larger than " + (group.fileSizeLimit || req.requestTenant.fileSizeLimit || Plan.getFileSizeLimit(plan)) });
                return;
            }
        } else {
            const fileSize = (await db.getConnection().query(`select sum(size) as sum from file where "registerId"=$1 and "groupId" IS NULL and "deletedAt" IS NULL`, [req.requestAccount.id]))[0].sum;
            if (new BigNumber(req.requestTenant.fileSizeLimit || Tenant.DEFAULT_FILE_SIZE_LIMIT_PER_ACCOUNT).lt(new BigNumber(fileSize).plus(new BigNumber(req.body.fileSize)))) {
                res.status(507).send({ error: "file size to be " + (new BigNumber(fileSize).plus(new BigNumber(req.body.fileSize))) + " is larger than " + (group.fileSizeLimit || Tenant.DEFAULT_FILE_SIZE_LIMIT_PER_ACCOUNT) });
                return;
            }
        }
        var uploadUrl = await Storage.getUploadUrl(req.requestTenant.key, Strings.randomString(50) + "/" + req.body.fileName, req.body.fileSize);
        var file = new File({ tenant: req.requestTenant, register: req.requestAccount, group: group, size: req.body.fileSize, name: req.body.fileName, url: uploadUrl.url + "/" + Strings.fixedEncodeURIComponent(uploadUrl.fields.key).replace(/%2F/g, "/") });
        await db.save(tableFile, file);
        uploadUrl.file = file;
        res.send(uploadUrl);
    })
    const getSignedUrl = async (req, res, groupId, url, callback, bypassJoinCheck) => {
        var group = null;
        if ("null" != groupId && "undefined" != groupId) {
            group = await Group.findGroupWithMember(req.requestTenant, groupId);
            if (!bypassJoinCheck && (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount))) {
                res.status(401).send();
                return;
            }
        }
        var file = await db.findOne(req.requestTenant, tableFile, "url=:url", { url });
        if (!file) {
            res.status(404).send();
            return;
        }
        if ("null" == groupId || "undefined" == groupId) {
            if (file.registerId != req.requestAccount.id) {
                res.status(401).send();
                return;
            }
        } else if (!group || group.id != file.groupId) {
            res.status(401).send();
            return;
        }

        callback(await Storage.getSignedUrl(req.requestTenant.key, url));
    };
    app.get('/api/groups/:id/downloadUrl', async function (req, res) {
        if (!req.params.id || !req.query.url) {
            res.status(400).send();
            return;
        }
        checkGroupAccessible(req, res, () => {
            getSignedUrl(req, res, req.params.id, req.query.url, async (url) => {
                res.send({ url });
            }, true)
        })
    })
    var checkGroupAccessible = async (req, res, callback) => {
        if (!req.params.id) {
            res.status(400).send();
            return;
        }
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.id);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        await callback(group);
    }
    app.get('/api/groups/:id/sprintBars', async function (req, res) {
        checkGroupAccessible(req, res, async (group) => {
            res.send(await SprintBar.loadSprintBar(req.requestTenant, group));
        })
    })
    app.get('/api/groups/:id/sprintBars/:sprintTitle', async function (req, res) {
        checkGroupAccessible(req, res, async (group) => {
            res.send(await SprintBar.loadSprintBarWithTitle(req.requestTenant, group, req.params.sprintTitle));
        })
    })
    app.post('/api/groups/:id/sprintBars', async function (req, res) {
        var title = req.body.title;
        var orderKey = req.body.orderKey;
        var holidays = req.body.holidays;
        if (!orderKey) {
            res.status(400).send();
            return;
        }
        checkGroupAccessible(req, res, async (group) => {
            const sprintBar = await db.save(tableSprintBar, { tenant: req.requestTenant, group, title, orderKey, holidays })
            const sprintBarEvent = new Event({ tenant: req.requestTenant, groupId: group.id, sprintBar, owner: req.requestAccount, type: EventType.SPRINT_CREATE, source: sprintBar });
            await db.save(tableEvent, sprintBarEvent)
            var pushEvent = Object.assign({ lastEvent: sprintBarEvent.summary }, sprintBar);
            ChatSender.sendToGroup(ChatSender.type.UPDATE_SPRINTBAR, pushEvent);

            res.send(sprintBar);
        })
    })
    app.put('/api/groups/:id/sprintBars', async function (req, res) {
        var id = req.body.id;
        var title = req.body.title;
        if (!title) {
            res.status(400).send();
            return;
        }
        checkGroupAccessible(req, res, async (group) => {
            var sprintBar = await db.findOne(req.requestTenant, tableSprintBar, `id = :id and "groupId" = :groupId`, { id, groupId: group.id })
            if (!sprintBar) {
                res.status(404).send();
                return;
            }
            sprintBar.title = title;
            sprintBar.startAt = Strings.toDate(req.body.startAt);
            sprintBar.endAt = Strings.toDate(req.body.endAt);
            if (req.body.holidays) {
                sprintBar.holidays = req.body.holidays;
            }
            res.send(await db.save(tableSprintBar, sprintBar))
        });
    })
    app.delete('/api/groups/:id/sprintBars/:sprintId', async function (req, res) {
        var id = req.params.sprintId;
        checkGroupAccessible(req, res, async (group) => {
            var sprintBar = await db.findOne(req.requestTenant, tableSprintBar, `id = :id and "groupId" = :groupId`, { id, groupId: group.id })
            if (!sprintBar) {
                res.status(404).send();
                return;
            }
            await db.delete(tableSprintBar, sprintBar);
            res.send();
            //TODO saveEvent?
            if (sprintBar.groupId) {
                ChatSender.sendToGroup(ChatSender.type.DELETE_SPRINTBAR, { group, id: sprintBar.id });
            }
        });
    })
    app.post('/api/groups', async function (req, res) {
        var uploadedGroup = req.body;
        if (!uploadedGroup.name || !uploadedGroup.key || !/^[A-Z\-0-9]+$/.test(uploadedGroup.key) || !uploadedGroup.members || uploadedGroup.members.length == 0) {
            res.status(400).send();
            return;
        }
        const plan = await Plan.loadPlan(req.requestTenant);
        var groups = await db.find(req.requestTenant, table, '"tenantId" = :tenantId', { tenantId: req.requestTenant.id });
        if ((req.requestTenant.groupCountLimit || Plan.getGroupCountLimit(plan)) <= groups.length) {
            res.status(507).send({ error: "group count is limited to " + (req.requestTenant.groupCountLimit || Plan.getGroupCountLimit(plan)), limit: (req.requestTenant.groupCountLimit || Plan.getGroupCountLimit(plan)) });
            return;
        }
        var group = {
            name: uploadedGroup.name,
            key: uploadedGroup.key,
            description: uploadedGroup.description,
            members: uploadedGroup.members,
            viewMembers: uploadedGroup.viewMembers,
            customAttributes: uploadedGroup.customAttributes,
            tenant: req.requestTenant,
            secret: uploadedGroup.secret,
            integrations: uploadedGroup.integrations
        }
        if (isUrl(uploadedGroup.iconUrl)) {
            group.iconUrl = uploadedGroup.iconUrl;
        }
        if (uploadedGroup.todoTemplate && uploadedGroup.todoTemplate.content) {
            group.todoTemplate = await db.save(tableTodoTemplate, { content: uploadedGroup.todoTemplate.content })
        }
        if (group.customAttributes && group.customAttributes.length > 0) {
            group.customAttributes.forEach((ca, index) => {
                delete ca.id;
                delete ca.groupId;
                delete ca.group;
                ca.order = index;
            });
            await db.save(tableCustomAttribute, group.customAttributes);
        }
        if (group.integrations && group.integrations.length > 0) {
            group.integrations.forEach(i => {
                delete i.id;
                delete i.groupId;
                delete i.group;
                i.register = req.requestAccount;
                i.tenantId = req.requestTenant.id;
            });
            await db.save(tableIntegration, group.integrations);
        }
        try {
            var generatedGroup = await db.save(table, group);
            await Group.createTodoSequence(req.requestTenant, generatedGroup, uploadedGroup.startSequence);
        } catch (e) {
            if ((e + "").indexOf("duplicate") >= 0) {
                res.status(409).send();
            } else {
                res.status(500).send();
            }
            return;
        }
        res.send(group);
    })
    app.put('/api/groups', async function (req, res) {
        var uploadedGroup = req.body;
        if (!uploadedGroup.name || !uploadedGroup.id) {
            res.status(400).send();
        }
        var group = await Group.findGroupWithMember(req.requestTenant, uploadedGroup.id);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        group.name = uploadedGroup.name;
        group.description = uploadedGroup.description;
        if (isUrl(uploadedGroup.iconUrl)) {
            group.iconUrl = uploadedGroup.iconUrl;
        }
        if (uploadedGroup.members) {
            group.members = uploadedGroup.members;
        }
        if (uploadedGroup.viewMembers) {
            group.viewMembers = uploadedGroup.viewMembers;
        }
        if (uploadedGroup.customAttributes) {
            var ids = uploadedGroup.customAttributes.filter(c => c.id).map(c => c.id);
            if (ids.length > 0) {
                var customAttributes = await db.find(null, tableCustomAttribute, `id in (:...ids)`, { ids });
                if (customAttributes.some(c => c.groupId != uploadedGroup.id)) {
                    res.status(401).send();
                    return;
                }
            }
            group.customAttributes = uploadedGroup.customAttributes;
        }
        if (uploadedGroup.todoTemplate && uploadedGroup.todoTemplate.content) {
            await db.createQueryBuilder(tableTodoTemplate)
                .delete()
                .from(tableTodoTemplate)
                .where(`"groupId" = :groupId`, { groupId: group.id })
                .execute();
            group.todoTemplate = await db.save(tableTodoTemplate, { content: uploadedGroup.todoTemplate.content })
        }
        if (uploadedGroup.integrations && uploadedGroup.integrations.length >= 0) {
            var ids = uploadedGroup.integrations.filter(c => c.id).map(c => c.id);
            if (ids.length > 0) {
                const integrations = await db.find(req.requestTenant, tableIntegration, `id in (:...ids)`, { ids })
                if (integrations.some(i => i.groupId != uploadedGroup.id)) {
                    res.status(401).send();
                    return;
                }
            }
            group.integrations = uploadedGroup.integrations;
        }
        if (group.customAttributes) {
            var cap = group.customAttributes.map((ca, index) => { ca.order = index; return ca }).map(async ca => await db.save(tableCustomAttribute, ca));
            await Promise.all(cap);
        }
        if (group.integrations && group.integrations.length > 0) {
            group.integrations.forEach(i => {
                i.groupId = group.id;
                i.tenantId = req.requestTenant.id;
                if (!i.registerId) {
                    i.registerId = req.requestAccount.id;
                }
            });
            await db.save(tableIntegration, group.integrations);

        }
        await db.save(table, group);
        res.send(group);
    })
    const maintenanceMember = async (req, res, join) => {
        if (!req.params.id) {
            res.status(400).send();
            return;
        }
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.id);
        if (!group || group.secret) {
            res.status(404).send();
            return;
        }
        const isMember = await Group.isMember(req.requestTenant, group.id, req.requestAccount, group);
        if (join) {
            if (isMember) {
                res.status(409).send();
                return;
            }
            if (req.body && req.body.type == GroupAccountRelationType.VIEWER) {
                group.viewMembers.push(req.requestAccount);
            } else {
                group.members.push(req.requestAccount);
            }
        } else {
            if (!isMember) {
                res.status(409).send();
                return;
            }
            group.members = group.members.filter(m => m.id != req.requestAccount.id);
            group.viewMembers = group.viewMembers.filter(m => m.id != req.requestAccount.id);
        }
        await db.save(table, group);
        res.send(group);
    }
    app.post('/api/groups/:id/join', async function (req, res) {
        await maintenanceMember(req, res, true)
    })
    app.post('/api/groups/:id/leave', async function (req, res) {
        await maintenanceMember(req, res, false)
    })
    app.get('/api/groups/:id/files', async function (req, res) {
        checkGroupAccessible(req, res, async (group) => {
            const files = await db.createQueryBuilder(tableFile)
                .leftJoinAndSelect(`${tableFile}.register`, `register`, `"register"."deletedAt" IS NULL`)
                .where(`${tableFile}."tenantId"=:tenantId and ${tableFile}."groupId"=:groupId and "${tableFile}"."deletedAt" IS NULL`, { tenantId: req.requestTenant.id, groupId: group.id })
                .orderBy(`"${tableFile}"."createdAt"`, "DESC")
                .getMany();
            res.send(files);
        })
    });
    app.delete('/api/groups/:id', async function (req, res) {
        const key = req.query.key;
        checkGroupAccessible(req, res, async (group) => {
            if (key != group.key) {
                res.status(400).send();
                return;
            }
            db.delete(table, group);
            res.send();
        })
    });
    app.post('/api/groups/:id/bulkUpdate', async function (req, res) {
        const info = req.body;
        checkGroupAccessible(req, res, async (group) => {
            try {
                const accountMap = [];
                group.members.forEach(m => accountMap[m.mail] = m);
                const clearAndSecurityConstant = (object, todo) => {
                    delete object.id;
                    object.group = group;
                    delete object.groupId;
                    object.tenant = req.requestTenant;
                    delete object.tenantId;
                    if (todo) {
                        object.todo = todo
                        delete object.todoId;
                    }
                }
                group.customAttributes.sort((c1, c2) => c1.order - c2.order);
                const treatTodo = (t, messageLinks, orders) => {
                    clearAndSecurityConstant(t);
                    if (t.personnels) {
                        t.personnels = t.personnels.map(p => accountMap[p.mail]).filter(p => p);
                    }
                    if (t.from) {
                        t.from = accountMap[t.from.mail];
                    }
                    var keyNumber = parseInt(t.key.substring(t.key.indexOf("-") + 1));
                    t.key = group.key + "-" + keyNumber;
                    if (t.messageLinks) {
                        t.messageLinks.forEach(m => {
                            clearAndSecurityConstant(m, t);
                            messageLinks.push(m);
                        });
                        delete t.messageLinks;
                    }
                    if (t.order) {
                        t.order.forEach(o => {
                            clearAndSecurityConstant(o, t);
                            orders.push(o);
                        });
                        delete t.order;
                    }
                    if (t.customAttributeValues) {
                        t.customAttributeValues.forEach(c => {
                            clearAndSecurityConstant(c, t);
                            customAttributeValues.push(c);
                            c.customAttribute = group.customAttributes[c.customAttribute.order];
                            customAttributeValues.push(c);
                        });
                        delete t.customAttributeValues;
                    }
                }
                const todos = info.todos;
                // first, set todo relation to child only.
                /// store child and relations 
                /// TBD how to escape duplication of relation???? 
                ///  > not duplicate caz top-level todo is not duplicate.and child not duplicate
                /// relation todo is not in info.json cas list not have relation.
                const children = [];
                const messageLinks = [];
                const orders = []
                const customAttributeValues = []
                todos.forEach(t => {
                    treatTodo(t, messageLinks, orders, customAttributeValues);
                    if (t.children) {
                        t.children.forEach(c => {
                            c.parent = t;
                            treatTodo(c, messageLinks, orders, customAttributeValues);
                            children.push(c);
                        });
                        delete t.children
                    }
                });
                if (todos.length > 0) {
                    await db.save(todoTable, todos);
                }
                if (children.length > 0) {
                    await db.save(todoTable, children);
                }
                if (messageLinks.length > 0) {
                    await db.save(tableMessageLink, messageLinks);
                }
                if (orders.length > 0) {
                    await db.save(tableTodoOrder, orders);
                }
                if (customAttributeValues.length > 0) {
                    await db.save(tableCustomAttributeValue, customAttributeValues);
                }
                if (info.files && info.files.length > 0) {
                    const files = [];
                    todos.forEach(t => {
                        t.files.forEach(f => {
                            files.push({ id: f.id, todo: t });
                        });
                        if (t.children) {
                            t.children.forEach(c => {
                                if (c.files) {
                                    c.files.forEach(f => files.push({ id: f.id, todo: t }))
                                }
                            })
                        }
                    });
                    const wrongFiles = await db.find(req.requestTenant, tableFile, `"groupId" != :groupId and id in (:...ids)`, { groupId: group.id, ids: files.map(f => f.id) });
                    if (wrongFiles.length != 0) {
                        throw "wrong file exists";
                    }
                    await db.save(tableFile, files);
                }

                if (info.bars && info.bars.length > 0) {
                    const bars = info.bars.map(b => {
                        clearAndSecurityConstant(b)
                        return b;
                    });
                    await db.save(tableSprintBar, bars);
                }
                if (info.wikis && info.wikis.length > 0) {
                    const wikis = info.wikis.map(w => {
                        clearAndSecurityConstant(w)
                        return w;
                    });
                    await db.save(tableWiki, wikis);
                }
                res.send();
            } catch (e) {
                console.log("error on restore " + e + ":" + e.stack);
                res.status(400).send();
            }
        })
    });
    const numJoinRegex = /^[,0-9]+$/
    app.get('/api/groups/:id/progressEvents', async function (req, res) {
        if (!req.query.todoIds || !numJoinRegex.test(req.query.todoIds)) {
            res.status(400).send();
            return;
        }
        const ids = req.query.todoIds.split(",");
        checkGroupAccessible(req, res, async (group) => {
            res.send(await Event.loadProgressEvent(req.requestTenant, group, ids));
        })
    })
}