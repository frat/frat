const redisAdapter = require('socket.io-redis');
const redis = require('redis');
const db = require("../util/db");
const { table: tableGroup } = require("../model/Group");
const { Account, table: tableAccount } = require("../model/Account");
const { PushedEvent, table: tablePushedEvent } = require("../model/PushedEvent");
const { table: tableTenant } = require("../model/Tenant");
const { table: tableAccountTenantRelation, role: AccountTenantRelationRole } = require("../model/AccountTenantRelation");
var _io;
var clustors
const forExport = function (io) {
    _io = io;
    const redishost = process.env.REDIS_HOST || 'localhost';
    const redisport = process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379;
    const redispass = process.env.REDIS_PASS || "password"

    const pub = redis.createClient(redisport, redishost, { password: redispass });
    const sub = redis.createClient(redisport, redishost, { password: redispass });
    io.adapter(redisAdapter({ pubClient: pub, subClient: sub }));
    clustors = [pub, sub];
    io.on('connection', (socket) => {
        socket.on("authenticate", async (joinRequest) => {
            var tenant = await db.findOne(null, tableTenant, "key=:key", { key: joinRequest.tenant });
            if (!tenant) {
                return;
            }

            const accountAccessKey = await Account.loadAccountAccessKey(joinRequest.accessKey, tenant);
            if (!accountAccessKey) {
                return;
            }
            var selfRelation = await db.findOne(tenant, tableAccountTenantRelation, '"accountId"=:accountId and "tenantId"=:tenantId and "role" in (:...roles)', { accountId: accountAccessKey.owner.id, tenantId: tenant.id, roles: [AccountTenantRelationRole.OWNER, AccountTenantRelationRole.MEMBER] })
            if (!selfRelation) {
                return;
            }
            var account = await db.createQueryBuilder(tableAccount).leftJoinAndSelect(tableAccount + ".groups", tableGroup, `"tenantId"=:tenantId`, { tenantId: tenant.id })
                .where(`"${tableAccount}".id = :id and "${tableGroup}"."deletedAt" IS NULL`, { id: accountAccessKey.owner.id })
                .getOne();
            if (!account || !account.groups) {
                return;
            }
            account.groups.forEach(group => {
                socket.join("group_" + group.id)
            });
            socket.on("requestEventAfter", async (request) => {
                const greaterThanId = request.greaterThanId;
                if (account.groups.length == 0 || !greaterThanId) {
                    return;
                }
                events = await db.find(null, tablePushedEvent, 'id > :greaterThanId and "groupId" in (:...groupIds)', { greaterThanId, groupIds: account.groups.map(g => g.id) }, null, null, "id");
                socket.emit("responseEventAfter", events);
            });
            socket.emit("authenticated");
        });
    });
}
forExport.isActive = () => {
    _io.emit("ping");
}
forExport.sender = {
    type: {
        NEW: "NEW_MESSAGE",
        UPDATE: "UPDATE_MESSAGE",
        UPDATE_TODO: "UPDATE_TODO",
        DELETE_TODO: "DELETE_TODO",
        UPDATE_SPRINTBAR: "UPDATE_SPRINTBAR",
        DELETE_SPRINTBAR: "DELETE_SPRINTBAR",
        UPDATE_TODO_PROGRESS: "UPDATE_TODO_PROGRESS",
        SORTED: "SORTED"
    },
    async sendToGroup(type, chatMessage) {
        const pushedEvent = await db.save(tablePushedEvent, new PushedEvent({ groupId: chatMessage.group, type, jsonMessage: JSON.stringify(chatMessage) }));
        chatMessage.eventId = pushedEvent.id;
        _io.to("group_" + chatMessage.group.id).emit(type, chatMessage);
    }
}
forExport.shutdown = async () => {
    if (clustors) {
        clustors.forEach(c => c.quit());
    }
    _io.httpServer.close();
}
module.exports = forExport;