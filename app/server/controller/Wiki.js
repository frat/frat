const { table } = require("../model/Wiki");
const { Group } = require("../model/Group");
const { Event, table: tableEvent, type: EventType } = require("../model/Event");
const db = require("../util/db");

module.exports = function (app) {
    app.get('/api/groups/:groupId/wikis', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const content = req.query.content ? ",content" : "";
        var wikis = await db.getConnection().query(`SELECT id, title ${content} FROM "${table}" WHERE "groupId" = $1 and "deletedAt" is null order by title`, [group.id]);
        res.send(wikis);
    })
    app.get('/api/groups/:groupId/wikis/:wikiId', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const wikiInDb = await db.findById(table, req.params.wikiId);
        if (!wikiInDb || wikiInDb.groupId != group.id || wikiInDb.tenantId != req.requestTenant.id) {
            res.status(404).send();
            return;
        }
        res.send(wikiInDb);
    })
    app.delete('/api/groups/:groupId/wikis/:wikiId', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const wikiInDb = await db.findById(table, req.params.wikiId);
        if (!wikiInDb || wikiInDb.groupId != group.id || wikiInDb.tenantId != req.requestTenant.id) {
            res.status(404).send();
            return;
        }
        await db.delete(table, wikiInDb);
        res.send(wikiInDb);
    })
    app.post('/api/groups/:groupId/wikis', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const wiki = req.body;
        delete wiki.id;
        wiki.tenant = req.requestTenant;
        wiki.register = req.requestAccount;
        wiki.groupId = group.id;
        const registedWiki = await db.save(table, wiki);
        const event = new Event({ tenant: req.requestTenant, group, wiki: registedWiki, owner: req.requestAccount, type: EventType.WIKI_CREATE, source: registedWiki });
        await db.save(tableEvent, event)
        res.send(registedWiki);
    })
    app.put('/api/groups/:groupId/wikis', async function (req, res) {
        const wiki = req.body;
        if (!wiki.id) {
            res.status(400).send();
            return;
        }
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const wikiInDb = await db.findById(table, wiki.id);
        if (!wikiInDb || wikiInDb.groupId != group.id || wikiInDb.tenantId != req.requestTenant.id) {
            res.status(404).send();
            return;
        }
        const beforeWiki = Object.assign({}, wikiInDb);
        wikiInDb.title = wiki.title;
        wikiInDb.content = wiki.content;
        const registedWiki = await db.save(table, wikiInDb);
        const event = new Event({ tenant: req.requestTenant, group, wiki: registedWiki, owner: req.requestAccount, type: EventType.WIKI_UPDATE, source: registedWiki, before: beforeWiki });
        await db.save(tableEvent, event)
        res.send(registedWiki);
    })
    app.get('/api/groups/:groupId/wikis/:id/events', async function (req, res) {
        var group = await Group.findGroupWithMember(req.requestTenant, req.params.groupId);
        if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group)) {
            res.status(401).send();
            return;
        }
        const wikiInDb = await db.findById(table, req.params.id);
        if (!wikiInDb || wikiInDb.groupId != group.id || wikiInDb.tenantId != req.requestTenant.id) {
            res.status(404).send();
            return;
        }
        const events = await db.getConnection().query(`SELECT e.id as id, e.type as type, e.summary as summary, e."createdAt" as "createdAt", a.name as "ownerName" FROM "${tableEvent}" e left join account a on e."ownerId" = a.id WHERE e."wikiId" = $1 order by e."createdAt"`, [wikiInDb.id]);
        res.send(events);
    });
}