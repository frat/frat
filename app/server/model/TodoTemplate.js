const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "todo_template";
class TodoTemplate {
    constructor(deafultArgs) {
        //fields
        this.content = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    TodoTemplate,
    table,
    targetName: "TodoTemplate"
}

const { targetName: groupTargetName } = require("./Group")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: TodoTemplate,
    columns: {
        content: {
            type: "varchar"
        }
    },
    relations: {
        group: {
            target: groupTargetName,
            type: "one-to-one",
            cascade: false,
            inverseSide: "todoTemplate",
            joinColumn: {
                name: 'groupId',
                referencedColumnName: 'id'
            }
        }
    }
}, DefaultEntity));