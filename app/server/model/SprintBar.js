const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const db = require("../util/db")
const table = "sprint_bar";
class SprintBar {
    constructor(deafultArgs) {
        //fields
        this.title = null;
        this.orderKey = null;
        this.startAt = null;
        this.endAt = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    SprintBar,
    table,
    targetName: "SprintBar"
}

const { targetName: accountTargetName } = require("../model/Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: tenantTargetName } = require("./Tenant");
SprintBar.loadSprintBar = async (tenant, group, limit, offset, orderBy) => {
    return await db.find(tenant, table, `"groupId" = :groupId and "deletedAt" IS NULL`, { groupId: group.id }, limit, offset, orderBy);
}
SprintBar.loadSprintBarWithTitle = async (tenant, group, sprintTitle) => {
    return await db.findOne(tenant, table, `"groupId" = :groupId and "deletedAt" IS NULL and title = :title`, { groupId: group.id, title: sprintTitle }, null, null, '"orderKey" DESC');
}
SprintBar.loadSelfSprintBar = async (tenant, account, limit, offset, orderBy) => {
    return await db.find(tenant, table, `"accountId" = :accountId and "deletedAt" IS NULL`, { accountId: account.id }, limit, offset, orderBy)
}
SprintBar.loadSprintOrderBetween = async (tenant, group, title) => {
    var bars = await db.getConnection().query(`SELECT s."orderKey" FROM "${table}" s WHERE s."tenantId" = $1 and s."groupId" = $2 and s."orderKey" <= (SELECT "orderKey" from "${table}" where "tenantId" = $1 and "groupId" = $2 and title = $3 and "deletedAt" is null) order by "orderKey" desc limit 2`, [tenant.id, group.id, title]);
    if (!bars[0]) {
        bars[0] = { orderKey: Number.MAX_VALUE };
    }
    if (!bars[1]) {
        bars[1] = { orderKey: 0.0 }
    }
    return {
        maxOrderKey: bars[0].orderKey,
        minOrderKey: bars[1].orderKey
    }
}

module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: SprintBar,
    columns: {
        title: {
            type: "varchar",
            nullable: true
        },
        orderKey: {
            type: "varchar"
        },
        startAt: {
            type: "timestamp",
            nullable: true
        },
        endAt: {
            type: "timestamp",
            nullable: true
        },
        accountId: {
            type: "int",
            nullable: true
        },
        groupId: {
            type: "int",
            nullable: true
        },
        holidays: {
            type: "varchar",
            nullable: true
        }
    },
    relations: {
        account: {
            target: accountTargetName,
            type: "many-to-one",
            joinColumn: {
                name: 'accountId',
                referencedColumnName: 'id'
            },
            cascade: false,
            nullable: true,
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            joinColumn: {
                name: 'groupId',
                referencedColumnName: 'id'
            },
            cascade: false,
            nullable: true
        },
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false
        },
    }
}, DefaultEntity));
