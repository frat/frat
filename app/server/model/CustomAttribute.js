const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "custom_attribute";
class CustomAttribute {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.title = null;
        this.description = null;
        this.type = null;
        this.candidates = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    CustomAttribute,
    table,
    targetName: "CustomAttribute",
    type: {
        TEXT: "TEXT",
        TEXTAREA: "TEXTAREA",
        INT: "INT",
        SELECT: "SELECT",
        DATE: "DATE",
        //       ACCOUNT: "ACCOUNT",
    }
}

const { targetName: groupTargetName } = require("./Group")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: CustomAttribute,
    columns: {
        title: {
            type: "varchar"
        },
        description: {
            type: "varchar",
            nullable: true
        },
        type: {
            type: "varchar"
        },
        candidates: {
            type: "varchar",
            nullable: true
        },
        order: {
            type: "int"
        },
        groupId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: "customAttributes",
        }
    }
}, DefaultEntity));