const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "pushed_event";
class PushedEvent {
    constructor(deafultArgs) {
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    PushedEvent,
    table,
    targetName: "PushedEvent"
}
const { targetName: groupTargetName } = require("../model/Group");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: PushedEvent,
    columns: {
        type: {
            type: "varchar"
        },
        groupId: {
            type: "int",
            nullable: true
        },
        jsonMessage: {
            type: "varchar",
            nullable: true
        }
    },
    relations: {
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true,
            inverseSide: 'files'
        }
    },
    indices: [{ unique: false, columns: ["group"] }]
}, DefaultEntity));