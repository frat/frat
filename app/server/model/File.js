const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "file";
class File {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.name = null;
        this.size = null;
        this.url = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    File,
    table,
    targetName: "File"
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: todoTargetName } = require("../model/Todo");
const { targetName: tenantTargetName } = require("./Tenant");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: File,
    columns: {
        name: {
            type: "varchar"
        },
        size: {
            type: "int"
        },
        url: {
            type: "varchar"
        },
        registerId: {
            type: "int"
        },
        groupId: {
            type: "int",
            nullable: true
        },
        todoId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false
        },
        register: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true,
            inverseSide: 'files'
        },
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true,
            inverseSide: 'files'
        },
    },
    indices: [{ unique: false, columns: ["todo"] }]
}, DefaultEntity));