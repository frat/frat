const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "todo_order";
class TodoOrder {
    constructor(deafultArgs) {
        //fields
        this.type = null;
        this.orderKey = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    TodoOrder,
    table,
    targetName: "TodoOrder",
    type: {
        SELF: "self",
        GROUP: "group"
    }
}

const { targetName: accountTargetName } = require("../model/Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: todoTargetName } = require("../model/Todo");

module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: TodoOrder,
    columns: {
        type: {
            type: "varchar"
        },
        orderKey: {
            type: "varchar"
        }
    },
    relations: {
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            joinColumn: {
                name: 'todoId',
                referencedColumnName: 'id'
            },
            cascade: false,
            inverseSide: 'order'
        },
        account: {
            target: accountTargetName,
            type: "many-to-one",
            joinColumn: {
                name: 'accountId',
                referencedColumnName: 'id'
            },
            cascade: false,
            nullable: true,
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            joinColumn: {
                name: 'groupId',
                referencedColumnName: 'id'
            },
            cascade: false,
            nullable: true
        }
    },
    indices: [{ unique: false, columns: ["todo"] }]
}, DefaultEntity));
