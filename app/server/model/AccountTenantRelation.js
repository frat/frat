const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "account_tenant_relation";
class AccountTenantRelation {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.role = 0;
        this.account = 0;
        this.tenant = 0;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    AccountTenantRelation,
    table,
    targetName: "AccountTenantRelation",
    role: { OWNER: "OWNER", MEMBER: "MEMBER", REQUESTER: "REQUESTER", INVITER: "INVITER", REJECTOR: "REJECTOR" }
}

const { targetName: accountTargetName } = require("./Account")
const { targetName: tenantTargetName } = require("./Tenant")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: AccountTenantRelation,
    columns: {
        role: {
            type: "varchar"
        }
    },
    relations: {
        account: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false,
            inverseSide: "tenantRelations"
        },
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false,
            inverseSide: "accountRelations"
        }
    }
}, DefaultEntity));