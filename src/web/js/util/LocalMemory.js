const LOCAL_STORAGE_PREFIX = "todo_localedited_cache"
const LOCAL_STORAGE_PREFIX_NEW_TODO = LOCAL_STORAGE_PREFIX + "_new";
export default {
    hasTodo(todo) {
        return JSON.parse(window.localStorage.getItem(this._getCachedId(todo)));
    },
    cacheTodo(todo) {
        window.localStorage.setItem(this._getCachedId(todo), JSON.stringify(todo));
    },
    clearCache(todo) {
        window.localStorage.removeItem(this._getCachedId(todo));
    },
    _getCachedId(todo) {
        return todo.id ? LOCAL_STORAGE_PREFIX + "_" + todo.id : LOCAL_STORAGE_PREFIX_NEW_TODO;
    }
}