import * as Axios from 'axios';
const KEY_AUTHENTICATION_KEY = "AuthenticationKey";

const CONFIG = {
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
    },
    responseType: 'json'
};

const client = Axios.default.create(CONFIG);
client.setAuthenticationKey = (key) => {
    localStorage[KEY_AUTHENTICATION_KEY] = key;
}
client.getAuthenticationKey = () => {
    return localStorage[KEY_AUTHENTICATION_KEY];
}

const setNoCache = (config) => {
    if (!config.params) {
        config.params = {};
    }
    if (typeof URLSearchParams === 'function' && config.params instanceof URLSearchParams) {
        config.params.append('_', Date.now());
    } else {
        config.params._ = Date.now();
    }
};
const appendsAuthenticationHeader = (config) => {
    if (localStorage[KEY_AUTHENTICATION_KEY]) {
        config.headers["Authentication"] = localStorage[KEY_AUTHENTICATION_KEY];
    }
};
client.interceptors.request.use((config) => {
    if (!config.url.match(/^https?:\/\//)) {
        setNoCache(config);
        appendsAuthenticationHeader(config);
    }
    return config;
});
client.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    console.log(error.response);
    return Promise.reject(error);
});
export default client;
