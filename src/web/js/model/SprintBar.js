import http from "../util/Http"

export async function loadSelfSprintBar(groupId) {
    return (await http.get("/api/accounts/self/sprintBars")).data;
}
export async function loadSprintBar(groupId, sprintTitle) {
    if (sprintTitle) {
        return (await http.get("/api/groups/" + groupId + "/sprintBars/" + encodeURIComponent(sprintTitle))).data;
    } else {
        return (await http.get("/api/groups/" + groupId + "/sprintBars")).data;
    }
}
export async function saveSelf(sprintBar) {
    return (await http[sprintBar.id ? "put" : "post"]("/api/accounts/self/sprintBars", sprintBar)).data;
}
export async function saveGroup(groupId, sprintBar) {
    return (await http[sprintBar.id ? "put" : "post"]("/api/groups/" + groupId + "/sprintBars", sprintBar)).data;
}
export async function deleteSprint(sprintBar) {
    if (sprintBar.groupId) {
        return (await http.delete("/api/groups/" + sprintBar.groupId + "/sprintBars/" + sprintBar.id)).data;
    } else {
        return (await http.delete("/api/accounts/self/sprintBars/" + sprintBar.id)).data;
    }
}