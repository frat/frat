import http from "../util/Http"

export async function tenantExistsHasKey(key) {
    var responsedData = (await http.get("/api/tenants/keyExists/" + key)).data;
    return responsedData.exists == true;
}
export async function loadTenant() {
    return (await http.get("/api/tenants/current")).data;
}
export async function putMember(account) {
    return (await http.put('/api/tenants/accounts', account)).data;
}
export async function loadFiles() {
    return (await http.get('/api/tenants/files')).data;
}
export async function deleteFile(file) {
    return (await http.delete('/api/tenants/files/' + file.id)).data;
}
export async function loadAll() {
    return (await http.get('/api/tenants')).data;
}
export async function saveAccessibleIPRanges(tenant) {
    return (await http.put('/api/tenants/accessibleIpRanges', tenant)).data;
}
export async function updateLimits(tenant) {
    return (await http.put('/api/tenants/updateLimits', tenant)).data;
}
export async function loadCurrentPlan() {
    return (await http.get('/api/tenants/plan')).data;
}
export async function updatePlan(plan) {
    return (await http.put('/api/tenants/plan', plan)).data;
}
export async function deleteNotJoin(account) {
    return (await http.delete('/api/tenants/accounts/notJoin/' + account.id)).data;
}
export async function searchTodo(query) {
    return (await http.get('/api/tenants/todos/search' + query)).data;
}


