import http from "../util/Http"

class SocketWrapper {
    constructor(socket) {
        this.socket = socket;
        this.wildCardEmitter = (event, data) => {
            this.lastReceivedEventId = data.eventId;
            var someKeyListeners = this.listeners.get(event);
            if (!someKeyListeners) {
                return;
            }
            someKeyListeners.forEach(l => l(data));
        }
        this.listeners = new Map();
    }
    on(event, listener) {
        var someKeyListeners = this.listeners.get(event);
        if (!someKeyListeners) {
            this.listeners.set(event, someKeyListeners = []);
            this.socket.on(event, (data) => { this.wildCardEmitter(event, data) });
        }
        someKeyListeners.push(listener);
    }
    off(event, listener) {
        var someKeyListeners = this.listeners.get(event);
        someKeyListeners = someKeyListeners.filter(l => l != listener);
        if (someKeyListeners.length == 0) {
            this.listeners.delete(event);
            this.socket.off(event, this.wildCardEmitter);
        } else {
            this.listeners.set(event, someKeyListeners);
        }
    }
    emit(event, data) {
        this.wildCardEmitter(event, data);
    }
}


export function connect() {
    const socket = io();
    const request = {
        accessKey: http.getAuthenticationKey(),
        tenant: location.hostname.split(".")[0]
    }
    const socketWrapper = new SocketWrapper(socket);
    socket.on('connect', () => {
        socket.emit("authenticate", request);
    });
    socket.on('authenticated', () => {
        if (!socketWrapper.lastReceivedEventId) {
            return;
        }
        socket.emit("requestEventAfter", { greaterThanId: socketWrapper.lastReceivedEventId });
        socket.once("responseEventAfter", (events) => {
            events.forEach((event) => {
                socketWrapper.emit(event.type, JSON.parse(event.jsonMessage));
            });
        })
    });
    return socketWrapper;
}

export async function sendMessage(todo, message) {
    return (await http.post(`/api/todos/${todo.id}/chatMessage`, { content: message })).data;
}
export async function getMessage(todo) {
    return (await http.get(`/api/todos/${todo.id}/chatMessage`)).data;
}