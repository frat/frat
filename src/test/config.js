
module.exports = {
    localexec: !!process.env.ENDPOINT,
    endpoint(port) {
        return process.env.ENDPOINT || "http://localhost:" + port
    },
    headless: process.env.HEADLESS == "false" ? false : true,
    capture: process.env.HEADLESS == "true"
}