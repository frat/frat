const puppeteer = require('puppeteer');
const config = require("./config");
const util = require("./util");
const getPort = require('get-port');
const server = require("../../app/server");
const webpack = require('webpack');
const webpackConfig = require('../../webpack.config')
server.suspendFirstStart = true;

jest.setTimeout(50000);
jest.retryTimes(3)

var port;
var browser;
var page;
beforeAll(async () => {
    var startTime = new Date().getTime();
    await new Promise((res, rej) => {
        webpackConfig.mode = "development"// for jest set NODE_ENV to "test"
        webpack(webpackConfig, (err, stats) => {
            if (err || stats.hasErrors()) {
                console.log("webpack failed " + (err || stats));
                rej(err || stats.hasErrors());
                return;
            }
            console.log("webpack compiled cost " + (new Date().getTime() - startTime) + "ms");
            res();
        })
    })
    port = await getPort();
    await server.start(port);
    process.on('exit', async () => {
        if (browser) {
            try {
                await browser.close();
            } catch (ignore) { }
        }
    });
});

afterAll(async () => {
    try {
        console.log("afterAll");
        await util.sleep(3000, "wait for all execution exited.");
        await server.stop();
    } catch (e) {
        console.log("error on server stop " + e);
    }
});

beforeEach(async () => {
    browser = await puppeteer.launch({ headless: config.headless });
    page = await browser.newPage();
    page.setViewport({
        width: 1200,
        height: 900,
        deviceScaleFactor: 1,
    });
});

afterEach(async () => {
    try {
        if (page && config.capture) {
            await page.screenshot({ path: 'src/test/results/result.png', fullPage: true });
        }
    } catch (e) {
        console.log("error on capture " + e + ":" + e.stack);
    }
    try {
        await Promise.all((await browser.pages()).map(async p => {
            await util.logout(p);
            return p.close()
        }));
    } catch (e) {
        console.log("error on pageclose " + e + ":" + e.stack);
    }
    if (browser) {
        try {
            await browser.close();
        } catch (e) {
            console.log("on close browser " + e + ":" + e.stack);
        }
    }
});

describe('Login', () => {
    it("redirect if not login", async () => {
        await page.goto(config.endpoint(port) + "/group/1", { waitUntil: "networkidle2" });
        await page.waitFor("#toLoginButton")
        expect((await page.url()).endsWith("/landing")).toBeTruthy();
    })
    it('loginable', async () => {
        await util.login(page, port);
    }, 50000);
    it('cant login with wrong key', async () => {
        try {
            await util.login(page, port, { key: "__wrongkey" });
            throw "login successed with wrong key";
        } catch (success) {
        }
    }, 50000);
    it('cant login with wrong mail', async () => {
        await page.goto(config.endpoint(port), { waitUntil: "networkidle2" });
        await page.waitFor('#toLoginButton');
        await page.click('#toLoginButton');
        await page.focus("#email")
        await page.type('#email', "wrong mail address");
        await page.click('#sendKeyButton')
        const className = await (await page.$('#email').then(el => el.getProperty('className'))).jsonValue();
        expect(className).toBe("validate invalid");
    }, 50000);
});
const LARGE_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const randomString = (length, src) => {
    var result = []
    for (let i = 0; i < length; i++) {
        result.push((src || LARGE_CHAR)[Math.floor(Math.random() * (src || LARGE_CHAR).length)]);
    }
    return result.join("");
}
const createGroupTestData = () => {
    return {
        name: "testGroupName" + new Date().getTime(),
        key: "GROUPKEY-" + randomString(10),
        description: "testGroupDescription" + new Date().getTime(),
        iconUrl: "https://example.com/iconUrl" + new Date().getTime()
    }
}
describe("Group", () => {
    it("group creatable and deletable", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);

        await util.sleep(500, "for Wait load groups")
        const defaultGroupLength = (await page.$$(".group_menu .group_button")).length
        await util.createGroup(page, testGroup);
        await util.retry(async () => {
            expect((await page.$$(".group_menu .group_button")).length).toBe(defaultGroupLength + 1)
        })
        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_group_setting");
        await page.click(".group_button_borderd .menu_group_setting");
        await util.sleep(500, "for Wait display")
        await util.retry(async () => {
            expect(await util.getValue(page, "#name")).toBe(testGroup.name)
            expect(await util.getValue(page, "#description")).toBe(testGroup.description)
        })
        // expect(await util.getValue(page, "#iconUrl")).toBe(testGroup.iconUrl)
        await util.sleep(500, "for Wait display")
        await page.click("#deleteButton");
        await util.sleep(1000, "for Wait display")
        await page.waitFor('#modalInputText');
        await page.type('#modalInputText', testGroup.key);
        await page.click(".text-complete-button");
        await page.waitFor(3000, "for Wait display")

        await util.retry(async () => {
            expect((await page.$$(".group_menu .group_button")).length).toBe(defaultGroupLength)
        })
    }, 50000)

    it("group editable", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);

        await util.createGroup(page, testGroup);

        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_group_setting");
        await page.click(".group_button_borderd .menu_group_setting");
        await util.sleep(500, "for button clickable")
        await page.click("#editButton")
        await util.sleep(1000, "for page editable")
        const newGroupName = "newGroupName" + new Date().getTime();
        const newGroupDescription = "newGroupDescription" + new Date().getTime();
        await util.setText(page, '#name', newGroupName)
        await util.setText(page, '#description', newGroupDescription)
        await util.sleep(1000, "for case button not clickable by validator");
        await page.click('#createButton');
        await util.sleep(500, "for relaod");
        await page.waitFor("#name");
        expect(await util.getValue(page, "#name")).toBe(newGroupName)
        expect(await util.getValue(page, "#description")).toBe(newGroupDescription)
    }, 50000)

    it("Secret group", async () => {
        const testSecretGroup = createGroupTestData();
        testSecretGroup.secret = true;
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testSecretGroup);
        const todoInSecretGroup = await util.createTodo({ page });
        await util.sleep(5000, "wait for dismiss toast.");
        await util.createGroup(page, testGroup);

        await util.openNewTodoPageAtCurrentGroup(page);

        await page.waitFor("#title:not([disabled])");
        await util.putTextDirectry(page, "#title", todoInSecretGroup.title);
        await util.sleep(1000, "no talking box although wait.")
        expect((await page.$$(".talking-box")).length).toBe(0)
        await util.clickEnabled(page, "#saveButton")
        await page.waitFor("#editButton")
    }, 50000)

    it("todo other open group talking", async () => {
        const testSecretGroup = createGroupTestData();
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testSecretGroup);
        const todoInSecretGroup = await util.createTodo({ page });
        await util.sleep(5000, "wait for dismiss toast.");
        await util.createGroup(page, testGroup);

        await util.openNewTodoPageAtCurrentGroup(page);

        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", todoInSecretGroup.title);
        await util.sleep(1000, "no talking box although wait.")
        await page.waitFor(".talking-box");
        await util.retry(async () => {
            expect((await page.$$(".talking-box")).length).toBe(1)
        })
        await util.clickEnabled(page, "#saveButton")
        await page.waitFor("#editButton")
    }, 50000)

    it("bugfix: group list visible when edit todo in selfView", async () => {
        const testGroup = createGroupTestData();
        const account = await util.login(page, port);
        await util.createGroup(page, testGroup);
        await util.createTodo({ page, accountName: account.name });
        await page.waitFor(`a[target-group-key="self"]`)
        await page.click(`a[target-group-key="self"]`)
        await page.waitFor(".box_list .draggable:nth-child(1) .title")
        await page.click(".box_list .draggable:nth-child(1) .title");
        await page.waitFor("#editButton")

        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await util.retry(async () => {
            expect((await page.$$(".todo")).length).toBe(1)
        })
    }, 50000)

    it("group leave and join", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);

        await util.retry(async () => {
            expect((await page.$$(".group_button")).length).toBe(3)
        })

        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_group_setting");
        await page.click(".group_button_borderd .menu_group_setting");
        await page.waitFor("#editButton")
        await util.sleep(500, "wait for... i dont know.");
        await page.click("#editButton")
        await page.waitFor(".delete-button")
        await page.click(".delete-button")
        await page.waitFor("#createButton")
        await page.click("#createButton")
        await util.retry(async () => {
            expect((await page.$$(".group_button")).length).toBe(2)
        })
        //join
        await page.click(".group_add_button")
        await page.waitFor(".join-button")
        await page.click(".join-button")
        await util.retry(async () => {
            expect((await page.$$(".group_button")).length).toBe(3)
        })
        //leave
        await page.hover(".group_button_borderd");
        await page.waitFor(`.group_button_borderd .menu_leave_group`);
        await page.click(`.group_button_borderd .menu_leave_group`);
        await util.retry(async () => {
            expect((await page.$$(".group_button")).length).toBe(2)
        })

    }, 50000)
})

describe("Integration", () => {
    const webhook = async ({ selectIndex, createWebhookJson, createWebhookBody, sharedSecret, check }) => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);

        await page.click('#editButton')
        await page.waitFor(".integration-area .select-dropdown");
        await page.click(".integration-area .select-dropdown");
        await page.waitFor(`.integration-area li:nth-child(${selectIndex})`);
        await page.click(`.integration-area li:nth-child(${selectIndex})`);
        if (sharedSecret) {
            await page.waitFor('#sharedSecret')
            await util.setText(page, '#sharedSecret', sharedSecret);
            await util.sleep(500);
        }
        await page.click(".integration-area button");
        await util.sleep(500);
        var uri = await util.getText(page, ".integration-area table tr:nth-child(1) td:nth-child(2)");
        await page.click('#createButton')
        await page.waitFor('#editButton')
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.sleep(500, "for load group");

        await util.createTodo({ page, atHome: true });
        await page.waitFor(".draggable:nth-child(1) .key")
        var todoKey = await util.getText(page, ".draggable:nth-child(1) .key");
        var response = await new Promise(resolve => {
            var postParam;
            if (createWebhookJson) {
                postParam = {
                    uri,
                    headers: {
                        "Content-type": "application/json"
                    },
                    json: createWebhookJson(todoKey)
                }
            } else if (createWebhookBody) {
                postParam = {
                    uri,
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded"
                    },
                    form: createWebhookBody(todoKey)
                }
            }
            if (sharedSecret) {
                const bufSecret = Buffer.from(sharedSecret, "base64");
                var msgBuf = Buffer.from(JSON.stringify(postParam.json), 'utf8');
                var msgHash = "HMAC " + require("crypto").createHmac('sha256', bufSecret).update(msgBuf).digest("base64");
                postParam.headers['authorization'] = msgHash;
            }
            require('request').post(postParam, (error, res, body) => {
                resolve({ error, res, body });
            });;
        })
        expect(response.res.statusCode).toBe(200);
        await check();
        return {
            group: testGroup,
            todoKey,
        }
    }
    it("webhook azure devops", async () => {
        const selectIndex = 2;
        const createWebhookJson = (todoKey) => {
            return {
                "eventType": "git.pullrequest.updated",
                "detailedMessage": {
                    "text": `this commit closing ${todoKey}`
                },
                "resource": {
                    "status": "completed",
                }
            }
        }
        await webhook({
            selectIndex, createWebhookJson, check: async () => {
                var progress = await util.getText(page, ".progress_value_display");
                expect(progress).toBe(":100%")
            }
        });
    }, 50000)
    it("webhook gitlab", async () => {
        const selectIndex = 1;
        const createWebhookJson = (todoKey) => {
            return {
                "object_kind": "merge_request",
                "object_attributes": {
                    "description": `this commit closing ${todoKey}`,
                    "action": "merge"
                }
            }
        }
        await webhook({
            selectIndex, createWebhookJson, check: async () => {
                var progress = await util.getText(page, ".progress_value_display");
                expect(progress).toBe(":100%")
            }
        });
    }, 50000)
    it("webhook github", async () => {
        const selectIndex = 3;
        const createWebhookJson = (todoKey) => {
            return {
                "action": "closed",
                "pull_request": {
                    "body": `this commit closing ${todoKey}`,
                    "merged": true
                }
            }
        }
        await webhook({
            selectIndex, createWebhookJson, check: async () => {
                var progress = await util.getText(page, ".progress_value_display");
                expect(progress).toBe(":100%")
            }
        });
    }, 50000)
    it("webhook slack", async () => {
        const selectIndex = 4;
        const createWebhookBody = (todoKey) => {
            return {
                "token": "XXXXXXXXXXXXXXXXXXXXXX",
                "team_id": "T958XXXX34",
                "team_domain": "dodomain",
                "service_id": "8510000000",
                "channel_id": "SDFSDFD",
                "channel_name": "random",
                "timestamp": "1575633639.001700",
                "user_id": "USDFSDFE",
                "user_name": "ukiuni",
                "text": "frat linkto " + todoKey + " commented",
                "trigger_word": "linkto"
            }
        }
        await webhook({
            selectIndex, createWebhookBody, check: async () => {
                await page.waitFor(".messageLink");
                expect((await page.$$(".messageLink")).length).toBe(1);
            }
        });
    }, 50000);
    const createWebhookJsonForTeams = (todoKey) => {
        return {
            "type": "message",
            "id": "1574567690668",
            "timestamp": "2019-11-24T03:54:50.888Z",
            "localTimestamp": "2019-11-24T12:54:50.888+09:00",
            "serviceUrl": "https://smba.trafficmanager.net/apac/",
            "channelId": "msteams",
            "from": {
                "id": "29:asdfasdfasdfasdfasdfasdfasdf",
                "name": "ukiunin",
                "aadObjectId": "1asdfasdf-asdf-asdf-asdf-asdfasdfasdfasdf"
            },
            "conversation": {
                "isGroup": true,
                "id": "19:asdfasdfasdfasdfasda@thread.skype;messageid=asdfasdfasdfasd",
                "name": null,
                "conversationType": "channel",
                "tenantId": "2asdfasdf-asdf-asdf-asdf-asdfasdfasdfasdf"
            },
            "recipient": null,
            "textFormat": "plain",
            "attachmentLayout": null,
            "membersAdded": [],
            "membersRemoved": [],
            "topicName": null,
            "historyDisclosed": null,
            "locale": "ja-JP",
            "text": " <at>inspect</at>&nbsp;linkto " + todoKey + " this is comment.",
            "speak": null,
            "inputHint": null,
            "summary": null,
            "suggestedActions": null,
            "attachments": [
                {
                    "contentType": "text/html",
                    "contentUrl": null,
                    //    "content": "<div><div><span itemscope=\"\" itemtype=\"http://schema.skype.com/Mention\" itemid=\"0\">inspect</span>&nbsp;linkto FRAT-100</div>\n</div>",
                    "name": null,
                    "thumbnailUrl": null
                }
            ],
            "entities": [
                {
                    "type": "clientInfo",
                    "locale": "ja-JP",
                    "country": "JP",
                    "platform": "Mac"
                }
            ],
            "channelData": {
                "teamsChannelId": "19:asdfasdfasdfasdfasdfasdfasdfasdf@thread.skype",
                "teamsTeamId": "19:asdfasdfasdfasdfasdfasdfasdfasdf@thread.skype",
                "channel": {
                    "id": "19:asdfasdfasdfasdfasdfasdfasdfasdf@thread.skype"
                },
                "team": {
                    "id": "19:asdfasdfasdfasdfasdfasdfasdfasdf@thread.skype"
                },
                "tenant": {
                    "id": "asdfasdf-asdf-asdf-asdf-asdfasdfasdf"
                }
            },
            "action": null,
            "replyToId": null,
            "value": null,
            "name": null,
            "relatesTo": null,
            "code": null
        }
    }
    it("webhook teams", async () => {
        const selectIndex = 5;
        await webhook({
            selectIndex, createWebhookJson: createWebhookJsonForTeams, sharedSecret: "asdf", check: async () => {
                await page.waitFor(".messageLink");
                expect((await page.$$(".messageLink")).length).toBe(1);
            }
        });
    }, 50000)

    it("messageLink stay with save and deletable", async () => {
        const selectIndex = 5;
        const result = await webhook({
            selectIndex, createWebhookJson: createWebhookJsonForTeams, sharedSecret: "asdf", check: async () => {
                await page.waitFor(".messageLink");
                expect((await page.$$(".messageLink")).length).toBe(1);
                await util.clickEnabled(page, "#editButton");
                await util.clickEnabled(page, "#saveButton");
                expect((await page.$$(".messageLink")).length).toBe(1);//link remains with just save
                await util.clickEnabled(page, "#editButton");
                await page.waitFor(".messageLink-close");
                await util.clickEnabled(page, ".messageLink-close");
                await util.clickEnabled(page, "#saveButton");
                expect((await page.$$(".messageLink")).length).toBe(0);
            }
        });
        result
    }, 5000000)
});
xdescribe("Manual", () => {
    it("manual appendable", async () => {
        const testGroup = createGroupTestData();
        const testManual = {
            title: "manualTitle" + new Date().getTime(),
            content: "manualContent" + new Date().getTime(),
            url: "http://example.com/manualUrl" + new Date().getTime()
        }
        await util.login(page, port);

        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click("#menu_manage_manual")
        await page.waitFor("#title");
        await util.setText(page, "#title", testManual.title);
        await util.setText(page, "#manual_content", testManual.content);
        await util.setText(page, "#url", testManual.url);

        await page.click("#addManualButton")
        await util.sleep(1000, "for case button not clickable by validator");
        await util.textSould(page, "tr:nth-child(1) td:nth-child(3)", testManual.title);
    }, 50000)
})

describe("Todo", () => {
    createNewTodo = () => {
        const time = new Date().getTime();
        return {
            title: "testTitle" + time,
            content: "testContent" + time
        }
    }

    it("todo appendable and show in home and editable to self and edit at home", async () => {
        await util.login(page, port);
        const testTodo = createNewTodo();
        await page.waitFor(".todo-add-button")
        await util.sleep(500, "wait for display")
        await page.click(".todo-add-button");
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", testTodo.title);
        await util.setText(page, "#content", testTodo.content);

        await util.clickEnabled(page, "#saveButton");
        await page.waitFor("#editButton");

        expect(await util.getValue(page, "#title")).toBe(testTodo.title)
        await util.textSould(page, ".markdown", testTodo.content);

        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.textSould(page, ".box_list .draggable:last-child .title", testTodo.title);

        await page.click(".box_list .draggable:last-child .open-todo-handle")
        await page.waitFor("#title");
        expect(await util.getValue(page, "#title")).toBe(testTodo.title)
        await util.textSould(page, ".markdown", testTodo.content);

        const overWritedTitle = "overWritedTitle" + new Date().getTime();
        const overWritedContent = "overWritedContent" + new Date().getTime();
        await page.click("#editButton");

        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", overWritedTitle);
        await util.setText(page, "#content", overWritedContent);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor("#title");
        expect(await util.getValue(page, "#title")).toBe(overWritedTitle)
        await util.textSould(page, ".markdown", overWritedContent);

        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await page.waitFor(".box_list .draggable:nth-child(1) .title")
        await page.click(".box_list .draggable:nth-child(1) .title");
        await page.waitFor("#editButton")
        await page.click("#editButton")
        const reOverWritedTitle = "reOverWritedTitle" + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", reOverWritedTitle);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", reOverWritedTitle);
    }, 50000)

    it("todo appendable and show in home and editable to group and edit at home", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        const testTodo = createNewTodo();
        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await util.openNewTodoPageAtCurrentGroup(page);
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", testTodo.title);
        await util.setText(page, "#content", testTodo.content);

        await util.clickEnabled(page, "#saveButton");
        await util.sleep(500, "for Wait display")

        expect(await util.getValue(page, "#title")).toBe(testTodo.title)
        await util.textSould(page, ".markdown", testTodo.content);

        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await page.waitFor(".box_list .draggable:last-child .title")
        await util.textSould(page, ".box_list .draggable:last-child .title", testTodo.title);

        await page.click(".box_list .draggable:last-child .open-todo-handle")
        await util.sleep(500, "for Wait display")
        expect(await util.getValue(page, "#title")).toBe(testTodo.title)
        await util.textSould(page, ".markdown", testTodo.content);

        const overWritedTitle = "overWritedTitle" + new Date().getTime();
        const overWritedContent = "overWritedContent" + new Date().getTime();
        await page.click("#editButton");
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", overWritedTitle);
        await util.setText(page, "#content", overWritedContent);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor('#title[disabled="disabled"]');
        expect(await util.getValue(page, "#title")).toBe(overWritedTitle)
        await util.textSould(page, ".markdown", overWritedContent);

        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await page.click(".box_list .draggable:nth-child(1) .title");
        await page.click("#editButton")
        await page.waitFor("#title:not([disabled])");
        const reOverWritedTitle = "reOverWritedTitle" + new Date().getTime();
        await util.setText(page, "#title", reOverWritedTitle);
        await util.sleep(1000, "tobe save enabled");
        await util.clickEnabled(page, "#saveButton");

        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", reOverWritedTitle);
    }, 50000)

    it("todo appendable at home", async () => {
        const createdName = "otherAccount" + new Date().getTime();
        await util.login(page, port, { name: createdName, mail: createdName + "@example.com" });
        await page.waitFor(".todo-add-button")
        await page.click(".todo-add-button");
        const title = "title" + new Date().getTime();
        const content = "content" + new Date().getTime();
        const point = Math.floor(Math.random() * 10) + "";
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        await util.setText(page, "#content", content);
        await util.setText(page, "#point", point);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", title);
        await util.textSould(page, ".box_list .draggable:nth-child(1) .point", point);
    }, 50000)

    it("todo appends file", async () => {
        await util.login(page, port);
        await page.waitFor(".todo-add-button")
        await util.sleep(500, "wait for display")
        await page.click(".todo-add-button");
        const title = "title" + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        const filePath = "src/test/resources/image.png";
        const input = await page.$('input[type="file"]');
        await input.uploadFile(filePath);
        await page.waitFor(".file-area .file");
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".file-area .file");
    }, 50000)

    it("todo search", async () => {
        await util.login(page, port);
        await page.waitFor(".todo-add-button")
        await util.sleep(500, "wait for display")
        await page.click(".todo-add-button");
        const title = "1title" + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        await util.clickEnabled(page, "#saveButton");
        await util.sleep(500);

        await page.click(".todo-add-button");
        const title2 = "2title" + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title2);
        await util.clickEnabled(page, "#saveButton");
        await util.retry(async () => {
            expect((await page.$$(".draggable")).length).toBe(2)
        })
        await page.click(".search-button")
        await page.waitFor("#searchInput");
        await util.setText(page, "#searchInput", title2);
        return "skip caz now search not limit shown todos.";
        await util.retry(async () => {
            expect((await page.$$(".todo")).length).toBe(1)
        })
    }, 50000)

    it("todo talking", async () => {
        await util.login(page, port);
        const testGroup = createGroupTestData();
        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await page.waitFor(".todo-add-button");
        await util.sleep(500, "button clickable");
        await page.click(".todo-add-button");
        await page.waitFor("#title:not([disabled])");
        const title = "title " + new Date().getTime();
        const content = "content" + new Date().getTime();
        await util.setText(page, "#title", title);
        await util.setText(page, "#content", content);
        await util.clickEnabled(page, "#saveButton");
        await util.sleep(500, "for Wait display")
        await page.click(".todo-add-button");
        await page.waitFor("#title:not([disabled])");
        await util.putTextDirectry(page, "#title", "title");
        await page.waitFor(".talking-box")
        await util.sleep(500, "for Wait display")
        await page.click(".talking-box:nth-child(1) i");
        await page.waitFor(".relation_todo:nth-child(1)")
        await util.clickEnabled(page, "#saveButton");

        await page.waitFor(".draggable:nth-child(2) .open-todo-handle i")
        await page.click(".draggable:nth-child(2) .open-todo-handle i")
        await page.waitFor(".relation_todo:nth-child(1)")
    }, 50000)

    it("create multi member todo", async () => {
        const testGroup = createGroupTestData();
        const createdName = "otherAccount" + new Date().getTime();
        const otherAccount = await util.login(page, port, { name: createdName, mail: createdName + "@example.com" });
        await util.logout(page, port);
        const loginAccount = await util.login(page, port);
        const testTodo = createNewTodo();
        await util.createGroup(page, testGroup, [otherAccount]);
        await util.sleep(1000, "for Wait group appended")
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.sleep(1000, "for Wait group appended")

        await page.waitFor(".todo-add-button")
        await page.click(".todo-add-button")

        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", testTodo.title);
        await util.setText(page, "#content", testTodo.content);
        await page.click("#appendPersonnelsAllGroupMemberButton");
        await util.sleep(500, "for Wait group appended")
        await page.click("#createChildPerPersonnelsButton");
        await util.sleep(1000, "for regist")
        await page.waitFor(".box .children-pane .children-area")
        await util.sleep(1000);
        while ((await (await (await page.$$(".box .children-pane .children-area"))[0].getProperty('textContent')).jsonValue()).indexOf(loginAccount.mail.split("@")[0]) == -1) {
            await util.sleep(1000);
        }

        expect((await (await (await page.$$(".box .children-pane .children-area"))[0].getProperty('textContent')).jsonValue()).indexOf(loginAccount.mail.split("@")[0]) >= 0).toBeTruthy()
        expect((await (await (await page.$$(".box .children-pane .children-area"))[1].getProperty('textContent')).jsonValue()).indexOf(otherAccount.mail.split("@")[0]) >= 0).toBeTruthy()
    }, 90000)
    it("create child todo", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        await util.sleep(500, "for reload");
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.sleep(1000);//wait until currentGroup loaded.
        var todo1 = await util.createTodo({ page, atHome: true });
        await page.waitFor("#editButton")
        await page.click("#editButton")
        const newChildTitle = "new child title " + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await page.click(".child-append")
        await page.waitFor("#newChildTitle")
        await util.setText(page, "#newChildTitle", newChildTitle)
        await page.click(".child-create")
        await page.waitFor(".child_todo");
        await util.clickEnabled(page, "#saveButton");
        await util.sleep(1000);
        await page.waitFor(".children-area");
        await page.waitFor(".box .todo .title");
        await page.click(".box .todo .title");

        await util.retry(async () => {//go to home
            await page.waitFor(".child_todo");
            await util.sleep(1000);
            await page.click(".child_todo");
        });
        await page.waitFor(".moveModal .key");
        await page.click(".moveModal .key");
        await util.retry(async () => {
            expect((await page.$$(".key")).length).toBe(2)
        });

        await util.retry(async () => {//go to home
            await page.waitFor(".group_button_borderd")
            await page.click(".group_button_borderd")
            await util.sleep(1000);
            expect((await page.$$(".todo_box")).length).toBe(1)
        });
        await util.sleep(1000);//wait until currentGroup loaded.
        var todo2 = await util.createTodo({ page, atHome: true, withParent: todo1 });
        await util.retry(async () => {
            expect((await page.$$(".key-area a")).length).toBe(2)
        });
        await page.waitFor("#editButton")
        await page.click("#editButton")

        await page.waitFor(".delete-parent")
        await page.click(".delete-parent")
        await util.clickEnabled(page, "#saveButton");
        await util.retry(async () => {
            expect((await page.$$(".key-area a")).length).toBe(1)
        });
        await page.waitFor("#editButton")
        await page.click("#editButton")
        await util.setParent(page, todo1);
        await util.appendsChild(page, { title: newChildTitle });
        await util.clickEnabled(page, "#saveButton");
        await util.retry(async () => {
            expect((await page.$$(".key-area a")).length).toBe(2)
        });
        await util.retry(async () => {
            expect((await page.$$(".child_todo")).length).toBe(1)
        });
        await page.waitFor("#editButton")
        await page.click("#editButton")
        await page.waitFor(".child-close")
        await page.click(".child-close")
        await util.retry(async () => {
            expect((await page.$$(".child_todo")).length).toBe(0)
        });
        await util.clickEnabled(page, "#saveButton");
        await util.retry(async () => {
            expect((await page.$$(".child_todo")).length).toBe(0)
        });
    }, 950000)
    //if do below feature, parent to be overrided by other group. is not happy....
    xit("create child todo of other group", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        await util.sleep(500, "for reload");
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.sleep(1000);//wait until currentGroup loaded.
        var todo1 = await util.createTodo({ page, atHome: true });

        const testGroup2 = createGroupTestData();
        await util.createGroup(page, testGroup2);
        await util.sleep(500, "for reload");
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        var todo2 = await util.createTodo({ page, atHome: true });
        await page.waitFor("#editButton")
        await page.click("#editButton")
        await page.waitFor(".child-select")
        await page.click(".child-select")

        await page.waitFor(".moveModal #searchTodoInput");
        await util.putTextDirectry(page, ".moveModal #searchTodoInput", todo1.title);
        await page.click(".searchTodoButton")
        await page.waitFor(".searched-todo-area .searched-todo");
        await page.click(".searched-todo-area .searched-todo");
        await page.click("#saveButton")
    }, 950000)
    it("todo with custom attribute", async () => {
        const testGroup = createGroupTestData();
        const loginAccount = await util.login(page, port);
        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)

        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_group_setting");
        await page.click(".group_button_borderd .menu_group_setting");
        await page.waitFor("#editButton")
        await util.sleep(500, "wait for... i dont know.");
        await page.click("#editButton")
        await page.waitFor("#createButton")
        var t = new Date().getTime();
        var customInput = {
            text: { name: "NameText" + t, i: 1, value: "text" + t },
            textarea: { name: "NameTextarea" + t, i: 2, value: "textarea" + t },
            int: { name: "int" + t, i: 3, value: "100" },
            select: { name: "select" + t, i: 4, candidates: "select1, select2, select3", value: "text" + t, value: "select1" },
            date: { name: "date" + t, i: 5, value: "2019-05-20" }
        }
        for (key of Object.keys(customInput)) {
            await util.setText(page, "#newCustomAttributeTitle", customInput[key].name);
            await page.click(".custom-attribute-area .select-wrapper");
            await page.waitFor(`.custom-attribute-area .select-wrapper ul li:nth-child(${customInput[key].i}) span`, { visible: true })
            await page.click(`.custom-attribute-area .select-wrapper li:nth-child(${customInput[key].i}) span`);
            if (customInput[key].candidates) {

                await page.waitFor("#newCustomAttributeCandidates");
                await page.focus("#newCustomAttributeCandidates");
                await util.setText(page, "#newCustomAttributeCandidates", customInput[key].candidates);
                expect(await util.getValue(page, "#newCustomAttributeCandidates")).toBe(customInput[key].candidates)
            }
            await page.click(".custom-attribute-input");
            await page.waitFor(`.custom-attribute-box tbody tr:nth-child(${customInput[key].i})`);
        }
        await page.click("#createButton");
        await util.sleep(500, "wait for regist");

        const testTodo = createNewTodo();
        await util.openNewTodoPageAtCurrentGroup(page);
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", testTodo.title);
        var inputIndex = 0;
        for (key of Object.keys(customInput)) {//put value
            if (key == "select") {
                await page.click(`#customInput_${inputIndex} .select-wrapper`);
                await page.waitFor(`#customInput_${inputIndex} .select-wrapper ul li`);
                await page.click(`#customInput_${inputIndex} .select-wrapper li:nth-child(1) span`);
                inputIndex++;
            } else if (key == "date") {
                await util.setDate(page, `#customInput_${inputIndex++} input`, customInput[key].value);
            } else if (key == "textarea") {
                await util.setText(page, `#customInput_${inputIndex++} textarea`, customInput[key].value);
            } else {
                await util.setText(page, `#customInput_${inputIndex++} input`, customInput[key].value);
            }
        }

        await util.clickEnabled(page, "#saveButton");
        await page.waitFor("#editButton");

        //check value
        inputIndex = 0;
        for (key of Object.keys(customInput)) {
            await page.waitFor(`#customInput_${inputIndex}`);
            if (key == "select") {
                expect(await util.getValue(page, `#customInput_${inputIndex++} input`)).toBe(customInput[key].value)
            } else if (key == "date") {
                expect(await util.getValue(page, `#customInput_${inputIndex++} input`)).toBe(customInput[key].value)
            } else if (key == "textarea") {
                expect(await util.getValue(page, `#customInput_${inputIndex++} textarea`)).toBe(customInput[key].value)
            } else {
                expect(await util.getValue(page, `#customInput_${inputIndex++} input`)).toBe(customInput[key].value)
            }
        }
    }, 50000)
    it("Template", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)
        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_group_setting");
        await page.click(".group_button_borderd .menu_group_setting");
        await page.waitFor("#editButton")
        await util.retry(async () => {
            await page.click("#editButton")
            await page.waitFor("#createButton");
        })
        const template = "# dddddd template " + new Date().getTime();
        await util.setText(page, "#todoTemplate", template);
        await page.click("#createButton");

        await util.sleep(500, "for reflesh display.");
        await util.openNewTodoPageAtCurrentGroup(page);

        await page.waitFor("#content");
        expect(await util.getValue(page, "#content")).toBe(template)
        await util.setText(page, "#title", template);

        await util.clickEnabled(page, "#saveButton");
        await page.waitFor("#editButton");

    }, 50000)
    it("Diff unit test", async () => {
        const before = { title: "title1", content: "testcontent is fine.\nAnd it's fine.\nAnd sunny day.", customAttributeValues: [{ customAttribute: { type: "TEXTAREA", title: "description" }, stringValue: "それでいいのでは？\nそんなことない。" }, { customAttribute: { type: "SELECT", title: "question" }, stringValue: "そうか？" }, { customAttribute: { type: "INT", title: "order" }, intValue: 1 }] };
        const source = { title: "title2", content: "testcontent is fine.\nBut it's not fine.\nAnd sunny day.", customAttributeValues: [{ customAttribute: { type: "TEXTAREA", title: "description" }, stringValue: "それでいいのでは？\nそのとおり。" }, { customAttribute: { type: "SELECT", title: "question" }, stringValue: "そうか？" }, { customAttribute: { type: "INT", title: "order" }, intValue: 2 }] };
        const event = { type: "TODO_UPDATE", before, source };
        const Event = require("../../app/server/model/Event").Event;
        const summary = new Event(event).summary;
        const expectSummary = { "type": "TODO_UPDATE", "changed": [{ "title": "title", "from": "title1", "to": "title2" }, { "title": "content", "diff": [{ "count": 7, "value": "testcontent is fine.\n" }, { "count": 1, "removed": true, "value": "And" }, { "count": 1, "added": true, "value": "But" }, { "count": 5, "value": " it's " }, { "count": 2, "added": true, "value": "not " }, { "count": 9, "value": "fine.\nAnd sunny day." }] }, { "title": "order", "from": 1, "to": 2, "customAttribute": true }, { "title": "description", "diff": [{ "count": 2, "value": "それでいいのでは？\n" }, { "count": 1, "removed": true, "value": "そんなことない。" }, { "count": 1, "added": true, "value": "そのとおり。" }], "customAttribute": true }] };
        expect(summary).toEqual(expectSummary)
    });
    it("insert Todo", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        const todo1 = {
            title: "title" + new Date().getTime(),
            content: "content" + new Date().getTime(),
            point: "1" + (Math.floor(Math.random() * 10) + 1) + "0"
        }
        await util.createTodo({ page, todo: todo1 });
        const todo2 = {
            title: "title2" + new Date().getTime(),
            content: "content2" + new Date().getTime(),
            point: "1" + (Math.floor(Math.random() * 10) + 1) + "1"
        }
        await util.createTodo({ page, todo: todo2 });

        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await page.click(".box_list .draggable:nth-child(1) .title", { button: "right" })
        await page.waitFor(`.ctx-menu-container[style*="block"] .context-create-todo-after`);
        await page.click(`.ctx-menu-container[style*="block"] .context-create-todo-after`);

        const todo3 = {
            title: "title3" + new Date().getTime(),
        }
        await page.waitFor("#modalInputText");
        await util.sleep("500", "wait for inputtable")
        await util.setText(page, "#modalInputText", todo3.title);
        await page.click(".modal-footer .text-complete-button");
        await util.retry(async () => {
            const todos = await page.$$(".box .todo .title");
            expect(todos.length).toBe(3)
            await util.textSould(page, ".box_list .draggable:nth-child(1) .title ", todo1.title);
            await util.textSould(page, ".box_list .draggable:nth-child(2) .title", todo3.title);
            await util.textSould(page, ".box_list .draggable:nth-child(3) .title", todo2.title);
        })
    }, 50000)
    it("Delete Todo", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        const todo1 = {
            title: "title" + new Date().getTime(),
            content: "content" + new Date().getTime(),
            point: (Math.floor(Math.random() * 10) + 1) + "0"
        }
        await util.createTodo({ page, todo: todo1 });
        const todo2 = {
            title: "title2" + new Date().getTime(),
            content: "content2" + new Date().getTime(),
            point: (Math.floor(Math.random() * 10) + 1) + "1"
        }
        await util.createTodo({ page, todo: todo2 });
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.retry(async () => {
            expect((await page.$$(".todo")).length).toBe(2)
        })
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await page.click(".box_list .draggable:nth-child(1) .title", { button: "right" })
        await page.waitFor(`.ctx-menu-container[style*="block"] .context-delete`);
        await page.click(`.ctx-menu-container[style*="block"] .context-delete`);
        await page.waitFor(".modal-close.text-complete-button");
        await page.click(".modal-close.text-complete-button");
        await util.retry(async () => {
            expect((await page.$$(".todo")).length).toBe(1)
        })
    }, 50000)
    it("Create sprint", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        const todo1 = {
            title: "title" + new Date().getTime(),
            content: "content" + new Date().getTime(),
            point: (Math.floor(Math.random() * 10) + 1) + "0"
        }
        await page.waitFor(".group_button_borderd")
        await page.click(".group_button_borderd")
        await util.createTodo({ page, todo: todo1, atHome: true });
        await page.click(".bar-add-button");
        await page.waitFor("#modalInputText")
        await page.waitFor(".text-complete-button")
        await page.click(".text-complete-button")
        await page.waitFor(".draggable-bar")
        await page.waitFor(".sprint-icon")
        await page.click(".sprint-icon")
        await page.waitFor(".sprint-startat-input input")
        await util.putTextDirectry(page, ".sprint-startat-input input", "2020-03-10");
        await page.keyboard.press('ArrowRight');
        await page.click(".sprint-edit-complete-button")
        //TODO not work with headless // await util.textSould(page, ".sprint-span", "2020/03/10");
    }, 50000)
});
describe("Search", () => {
    it("search todo", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        const todo1 = {
            title: "title" + new Date().getTime(),
            content: "content" + new Date().getTime(),
            point: (Math.floor(Math.random() * 10) + 1) + "0"
        }
        await util.createTodo({ page, todo: todo1 });
        const todo2 = {
            title: "title2" + new Date().getTime(),
            content: "content2" + new Date().getTime(),
            point: (Math.floor(Math.random() * 10) + 1) + "1"
        }
        await util.createTodo({ page, todo: todo2 });

        await page.waitFor(`.group_button_borderd`);
        await page.hover(`.group_button_borderd`);
        await page.waitFor(`.group_button_borderd .menu_search`);
        await page.click(`.group_button_borderd .menu_search`);
        await page.waitFor("#point")
        await util.setText(page, "#point", todo2.point);
        await util.retry(async () => {
            await page.hover(".group_button_borderd");
            await page.waitFor("#searchButton")
            await page.click("#searchButton")
            await util.retry(async () => {
                expect((await page.$$(".box .todo .title")).length).toBe(1)
            })
        });

    }, 50000)
});
describe("Wiki", () => {
    it("Wiki", async () => {
        const testGroup = createGroupTestData();
        await util.login(page, port);
        await util.createGroup(page, testGroup);
        await util.sleep(3000, "for group created.")
        await page.waitFor(".group_button_borderd");
        await page.hover(".group_button_borderd");
        await page.waitFor(".group_button_borderd .menu_wiki");
        await page.click(".group_button_borderd .menu_wiki");

        await page.waitFor("#wikiAddButton")
        await util.retry(async () => {
            await page.click("#wikiAddButton")
            await page.waitFor("#title:not([disabled])");
        })
        const title = "1new Title" + new Date().getTime();
        const content = "1new Content" + new Date().getTime();
        await util.setText(page, "#title", title);
        await util.setText(page, "#content", content);
        await util.clickEnabled(page, "#saveButton");
        await util.textSould(page, ".wiki-in-list:nth-child(1) .title", title);
        await util.sleep(500);
        await page.click("#wikiAddButton")
        await page.waitFor("#title:not([disabled])");


        const title2 = "2new Title" + new Date().getTime();
        await util.retry(async () => {
            await util.setText(page, "#title", title2);
        })
        await util.clickEnabled(page, "#saveButton");
        await util.textSould(page, ".wiki-in-list:nth-child(1) .title", title);
        await util.textSould(page, ".wiki-in-list:nth-child(2) .title", title2);

        //delete
        await page.click(".wiki-in-list:nth-child(1) .title", { button: "right" })
        await page.waitFor(".context-delete");
        await page.click(".context-delete");
        await page.waitFor(".text-complete-button");
        await page.click(".text-complete-button");
        await util.sleep(1000);
        await util.textSould(page, ".wiki-in-list:nth-child(1) .title", title2);
    }, 50000)
});
describe("Space", () => {
    const createSpace = async () => {
        const spaceName = randomString(10, "abcdefghijklmnopqrstubwxyz")
        await page.goto(config.endpoint(port) + "/space/new", { waitUntil: "networkidle2" });
        await page.waitFor("#subdomain");
        await util.setText(page, "#subdomain", spaceName);
        await page.click("#createButton");
        await page.waitFor("#email");
    }
    it("In new space, request and join", async () => {
        //default space
        const createdName = "firstAccount" + new Date().getTime();
        await util.login(page, port, { name: createdName, mail: createdName + "@example.com" });

        const testGroup = createGroupTestData();

        await util.createGroup(page, testGroup);

        await page.waitFor(".group_button_borderd");
        await page.click(".group_button_borderd");
        await page.waitFor(".todo-add-button")
        await util.sleep(500, "wait for display")
        await page.click(".todo-add-button");

        const title = "title" + new Date().getTime();
        const content = "content" + new Date().getTime();
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        await util.setText(page, "#content", content);
        await util.sleep(500, "button enabled")
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", title);

        //other space
        await createSpace();
        await util.login(page, port, { name: createdName, mail: createdName + "@example.com", withCurrentPath: true });

        //group in other space not visible
        await util.retry(async () => {
            const defaultGroupLength = (await page.$$(".group_menu .group_button")).length
            expect(defaultGroupLength).toBe(2);//self and append.
        })

        await util.createGroup(page, testGroup);
        await page.waitFor(`a[target-group-key="group_${testGroup.key}"]`)
        await page.click(`a[target-group-key="group_${testGroup.key}"]`)

        await page.waitFor(".todo-add-button");
        await util.sleep(500, "wait for clickable");
        await page.click(".todo-add-button");
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        await util.setText(page, "#content", content);

        await util.sleep(2000, "for Wait display");
        expect(await page.$('.talking-box')).toBe(null);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", title);

        await page.waitFor(".todo-add-button")
        await page.click(".todo-add-button");
        await page.waitFor("#title:not([disabled])");
        await util.setText(page, "#title", title);
        await util.clickEnabled(page, "#saveButton");
        await page.waitFor(".box_list .draggable:nth-child(1) .title");
        await util.textSould(page, ".box_list .draggable:nth-child(1) .title", title);
        await util.logout(page);

        //other account
        const secondName = "secondAccount" + new Date().getTime();
        await util.login(page, port, { name: createdName, mail: secondName + "@example.com", withCurrentPath: true, suspectNotLogin: true });
        await page.waitFor("#requestingToJoinMessage");

        //authorize
        await page.evaluate(() => location.href = "/landing");
        await page.waitFor('#toLoginButton');
        await util.login(page, port, { name: createdName, mail: createdName + "@example.com", withCurrentPath: true });

        await page.waitFor(".header_dropdown_toggle");
        await page.click(".header_dropdown_toggle");
        await page.waitFor("#settingSpace");
        await page.click("#settingSpace");

        await page.waitFor(".role-select:nth-child(1) input");
        await page.click(".role-select:nth-child(1) input");
        await page.waitFor(".role-select:nth-child(1) li:nth-child(3)");
        await page.click(".role-select:nth-child(1) li:nth-child(3)");
        await util.sleep(1000);
        await util.logout(page);

        //other account loginable
        await util.login(page, port, { name: createdName, mail: secondName + "@example.com", withCurrentPath: true });
        await page.waitFor(".todo-add-button")
    }, 90000);

    it("Buy plan", async () => {
        const createdName = "firstAccount" + new Date().getTime();
        await createSpace();

        await util.login(page, port, { name: createdName, mail: createdName + "@example.com", withCurrentPath: true });

        await page.waitFor(".header_dropdown_toggle");
        await page.click(".header_dropdown_toggle");

        await page.waitFor("#settingSpace");
        await page.click("#settingSpace");

        await page.waitFor(".to-buy-plan");
        await page.click(".to-buy-plan");
        await page.waitFor('.using');
        await page.click('input[type="radio"][value="standard"]');
        await page.click("#createButton");
        const putCardNumber = async (card) => {
            await page.waitFor(".card-number");
            await util.setText(page, ".card-number:nth-child(1)", card ? card[0] : "4242");
            await util.setText(page, ".card-number:nth-child(2)", card ? card[1] : "4242");
            await util.setText(page, ".card-number:nth-child(3)", card ? card[2] : "4242");
            await util.setText(page, ".card-number:nth-child(4)", card ? card[3] : "4242");

            await page.select('select[name="cardLimitMonth"]', '12');
            await page.select('select[name="cardLimitYear"]', '30');
            await util.setText(page, ".secret-code", "000");

            await page.waitFor(".text-complete-button:not([disabled])");
            await page.click(".text-complete-button");
        }
        putCardNumber();

        await page.waitFor('.colord input[type="radio"][value="standard"]');
        await page.click('input[type="radio"][value="premium"]');
        await page.click("#createButton");
        putCardNumber();

        await util.retry(async () => {
            await util.sleep(5000);
            await page.waitFor('.colord input[type="radio"][value="premium"]');
        });

        await page.click('input[type="radio"][value="free"]');
        await page.click("#createButton");
        await page.waitFor(".text-complete-button");
        await page.click(".text-complete-button");

        await util.retry(async () => {
            await util.sleep(5000);
            await page.waitFor('.colord input[type="radio"][value="free"]');
        });

        await page.click('input[type="radio"][value="premium"]');
        await page.click("#createButton");
        putCardNumber(["4000", "0000", "0000", "0002"]);
        await page.waitFor('.moveModal .invalid');

        await util.sleep(10000);
    }, 90000);

    it("Invite and join", async () => {
        const createdName = "firstAccount" + new Date().getTime();
        await createSpace();
        await util.login(page, port, { name: createdName, mail: createdName + "@example.com", withCurrentPath: true });

        //invite
        await page.waitFor(".header_dropdown_toggle");
        await page.click(".header_dropdown_toggle");
        await page.waitFor("#settingSpace");
        await page.click("#settingSpace");
        await page.waitFor("#newEmail input");
        const nextmail = "inviteAccount" + new Date().getTime() + "@example.com";
        await util.setText(page, "#newEmail input", nextmail);
        await page.click("#newEmail  button");
        await page.waitFor("tbody tr:nth-child(2)");
        await util.textSould(page, "tbody tr:nth-child(2)", nextmail);
        await util.logout(page);

        //join and loginable
        await page.evaluate(() => location.href = "/landing");
        await page.waitFor('#toLoginButton');
        await util.login(page, port, { mail: nextmail, withCurrentPath: true });
        await util.logout(page);

        //role changed
        await page.evaluate(() => location.href = "/landing");
        await page.waitFor('#toLoginButton');

        await util.login(page, port, { name: createdName, mail: createdName + "@example.com", withCurrentPath: true });
        await page.waitFor(".header_dropdown_toggle");
        await page.click(".header_dropdown_toggle");
        await page.waitFor("#settingSpace");
        await page.click("#settingSpace");
        await page.waitFor(`select option[value="MEMBER"]`);
    }, 50000);
});
describe("Monitor", () => {
    it("monitor api accessible", async () => {
        const url = require("./config").endpoint(port) + "/api/monitor";
        const response = await new Promise((resolve, rej) => {
            require('request').get({ url }, (error, res) => {
                if (error) {
                    rej(error);
                    return;
                }
                resolve(res);
            });;
        });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.body).status).toBe("ok");
    })
});
