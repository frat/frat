# fRat
Make us work better.

# Run
## precondition
* [Node.js](https://nodejs.org/ja/)
* [Vagrant](https://www.vagrantup.com/)
* [VirtualBox](https://www.virtualbox.org/)
are installed and port 3000(fRat) port 6379(Redis) port 5432(postgres) port 1025(MailHog SMTP) port 8025(MailHog WebIF) port 9000(minio)are not used.

## Run

```
vagrant up
```

postgres are boot on docker on VirtualBox.

```
npm install
npm run dev
```

fRat start at port 3000.
if change file, fRat will reboot.
Port 8025(MailHog) shows mails.

# Enviroments settings.
ADMIN_MAILS system admin mail address.
DB_DB db name
DB_HOST db host
DB_PASS db password
DB_USER db user name
DEFAULT_DOMAINS space name loginnable without invitation
MAIL_FROM mail address from system
MAIL_HOST mail server host
MAIL_PASS mail server password
MAIL_PORT mail server port
MAIL_USRE mail server user name
REDIS_HOST redis host name
REDIS_PASS redis password
S3_ACCESS_KEY s3 access key
S3_ENDPOINT s3 endpoint
S3_SECRET_KEY s3 secret key


# License
See ./LICENSE

# Copyright
ukiuni ukiuni@gmail.com
